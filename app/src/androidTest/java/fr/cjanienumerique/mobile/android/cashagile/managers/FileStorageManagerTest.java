package fr.cjanienumerique.mobile.android.cashagile.managers;

import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;

import fr.cjanienumerique.mobile.android.cashagile.enums.FileTarget;
import fr.cjanienumerique.mobile.android.cashagile.managers.FileStorageManager;

@RunWith(AndroidJUnit4.class)
public class FileStorageManagerTest {

    private final Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
    private FileStorageManager fileStorageManager;

    @Before
    public void instanciateFileStorageManager() {
        this.fileStorageManager = FileStorageManager.getInstance(this.context);
    }

    @Test
    public void testGetTargetFileName() {
        Assert.assertNotNull(this.fileStorageManager.getTargetFileName());
        Assert.assertEquals("sample", this.fileStorageManager.getTargetFileName());
        this.fileStorageManager.setFileTarget(FileTarget.SESSION);
        Assert.assertEquals("session", this.fileStorageManager.getTargetFileName());
        this.fileStorageManager.setFileTarget(null);
        Assert.assertEquals("sample", this.fileStorageManager.getTargetFileName());
    }

    @Test
    public void testGetTargetFilePath() {
        this.fileStorageManager.setFileTarget(FileTarget.SAMPLE);
        Assert.assertEquals(this.context.getFilesDir().toString() + "/text/sample", this.fileStorageManager.getTargetFilePath());
        this.fileStorageManager.setFileTarget(FileTarget.SESSION);
        Assert.assertEquals(this.context.getFilesDir().toString() + "/text/session", this.fileStorageManager.getTargetFilePath());
    }

    @Test
    public void testGetTargetFile() {
        this.fileStorageManager.setFileTarget(FileTarget.SAMPLE);
        File file = this.fileStorageManager.getTargetFile();
        Assert.assertNotNull(file);
        Assert.assertEquals("sample", file.getName());
        Assert.assertEquals(this.context.getFilesDir().toString() + "/text/sample", file.getPath());
    }
}
