package fr.cjanienumerique.mobile.android.cashagile.datalayers;

import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import javax.mail.internet.AddressException;

import fr.cjanienumerique.mobile.android.cashagile.dataLayers.DataLayer;
import fr.cjanienumerique.mobile.android.cashagile.entities.Company;

@RunWith(AndroidJUnit4.class)
public class DataLayerSessionTest {

    private final Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();

    private DataLayerSession dataLayerSession;




    @Before
    public void instanciateDataLayerSession() {
        this.dataLayerSession = DataLayerSession.getInstance(context);
    }

    @Test
    public void testGetSession() throws IOException, AddressException {
        Company company = new Company("Bateau", "88");
        this.dataLayerSession.saveSession(company);
        Assert.assertNotNull(this.dataLayerSession.getSession());
        Company load = this.dataLayerSession.getSession().getValue();
        Assert.assertNotNull(load);
        Assert.assertEquals("bateau", load.getName());
        Assert.assertEquals("88", load.getReference());
        Assert.assertNull(load.getEmail());
    }
}
