package fr.cjanienumerique.mobile.android.cashagile.factories;

import org.junit.Assert;
import org.junit.Test;

import fr.cjanienumerique.mobile.android.cashagile.entities.Command;
import fr.cjanienumerique.mobile.android.cashagile.entities.Item;
import fr.cjanienumerique.mobile.android.cashagile.exceptions.EmptyParameterException;
import fr.cjanienumerique.mobile.android.cashagile.exceptions.InvalidPriceException;
import fr.cjanienumerique.mobile.android.cashagile.exceptions.InvalidQuantityException;

public class CommandFactoryTest {

    @Test
    public void create() throws InvalidQuantityException, InvalidPriceException, EmptyParameterException {
        Command command = CommandFactory.getInstance().create("2", "web site", "2000");
        Assert.assertNotNull(command);
        Assert.assertEquals(2, command.getQuantity(), 0);
        Assert.assertEquals("web site", command.getItem().getName());
        Assert.assertEquals(2000, command.getItem().getPrice(), 0);
    }

    // Test exceptions

    @Test(expected = EmptyParameterException.class)
    public void whenEmptyParameterExceptionIsThrown_thenExpectationSatisfied() throws InvalidQuantityException, EmptyParameterException, InvalidPriceException {
        Command command = CommandFactory.getInstance().create("1", " ", "2");
    }

    @Test(expected = InvalidQuantityException.class)
    public void whenInvalidQuantityExceptionIsThrown_thenExpectationSatified() throws InvalidQuantityException, EmptyParameterException, InvalidPriceException {
        Command command = CommandFactory.getInstance().create("A", "café", "1");
    }

    @Test(expected = InvalidPriceException.class)
    public void whenInvalidPriceExceptionIsThrown_thenExpectationSatisfied() throws InvalidQuantityException, EmptyParameterException, InvalidPriceException {
        Command command4 = CommandFactory.getInstance().create("1", "café", "A");
    }
}
