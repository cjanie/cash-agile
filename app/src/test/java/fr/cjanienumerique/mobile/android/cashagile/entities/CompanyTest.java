package fr.cjanienumerique.mobile.android.cashagile.entities;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class CompanyTest {

    // Company to test **********

    private Company company;

    // Instanciate ***************

    @Before
    public void setUp() {
        this.company = new Company("Bateau", "99999");
        try {
            this.company.setEmail(new InternetAddress("a@a"));
        } catch (AddressException e) {
            e.printStackTrace();
        }
    }

    // Tests ***************

    // String formatCompanyToString()
    @Test
    public void testFormatCompanyToString() throws AddressException {
        Assert.assertEquals("BATEAU", this.company.formatCompanyToString());
    }

    // String formatClientToString()
    @Test
    public void testFormatClientToString() throws AddressException {
        Assert.assertEquals("BATEAU", this.company.formatClientToString());
    }

    // String formatSessionToString()
    @Test
    public void testFormatSessionToString() throws AddressException {
        Assert.assertEquals("BATEAU", company.formatSessionToString());
    }

    // Tests with Mock *********

    // InternetAddress getSessionEmail()
    @Test
    public void testGetSessionEmail() {
        // TODO
        InternetAddress email = this.company.getEmail();
        Assert.assertNotNull(email);
        Assert.assertEquals("a@a", email.getAddress());
        this.company.setEmail(null);
        Assert.assertNull(this.company.getEmail());
    }


    // void setSessionEmail(InternetAddress email)
    @Test
    public void testSetSessionEmail() throws AddressException {
        // TODO
        this.company.setSessionEmail(new InternetAddress("av@av"));
        Assert.assertEquals("av@av", this.company.getSessionEmail().getAddress());
        Assert.assertEquals("av@av", this.company.getEmail().getAddress());
    }


}
