package fr.cjanienumerique.mobile.android.cashagile.core;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ListOfStringTest {

    @Test
    public void testGetStringsColumnWidth() {
        List<String> strings = new ArrayList<>();
        strings.add("bla");
        strings.add("blabla");
        Assert.assertEquals(6, new ListOfStrings(strings).getStringsColumnWidth());
    }

    @Test
    public void testFormatStringsToContentOfGrid() {
        List<String> strings = new ArrayList<>();
        strings.add("bla");
        strings.add("blabla");
        Assert.assertEquals("bla   ", new ListOfStrings(strings).formatStringsToContentOfGrid().get(0));
    }
}
