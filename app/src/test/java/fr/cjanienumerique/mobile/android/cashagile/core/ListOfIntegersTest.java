package fr.cjanienumerique.mobile.android.cashagile.core;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ListOfIntegersTest {

    @Test
    public void testGetIntegersColumnWidth() {
        List<Integer> integers = new ArrayList<>() ;
        integers.add(2);
        integers.add(1000);
        integers.add(40);
        Assert.assertEquals(4, new ListOfIntegers(integers).getIntegersColumnWidth());
    }

    @Test
    public void testFormatIntegersToContentOfGrid() {
        List<Integer> integers = new ArrayList<>() ;
        integers.add(2);
        integers.add(1000);
        integers.add(40);
        Assert.assertEquals("   2", new ListOfIntegers(integers).formatIntegersToContentOfGrid().get(0));
        Assert.assertEquals("1000", new ListOfIntegers(integers).formatIntegersToContentOfGrid().get(1));
        Assert.assertEquals("  40", new ListOfIntegers(integers).formatIntegersToContentOfGrid().get(2));
    }
}
