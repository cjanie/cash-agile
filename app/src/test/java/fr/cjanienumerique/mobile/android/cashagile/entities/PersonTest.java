package fr.cjanienumerique.mobile.android.cashagile.entities;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import fr.cjanienumerique.mobile.android.cashagile.enums.Gender;

public class PersonTest {

    private Person anneMarie;

    private Person anna;

    @Before
    public void setUp() {
        this.anneMarie = new Person(Gender.MME, "anne-marie", "Lancienne");
        try {
            this.anneMarie.setEmail(new InternetAddress("a@a"));
        } catch (AddressException e) {
            e.printStackTrace();
        }
        this.anna = new Person(Gender.MME, "anna", "monfort");
    }

    // String formatFirstName()
    @Test
    public void testformatFirstName() {
        Assert.assertEquals("Anne-Marie", this.anneMarie.formatFirstName());
    }

    // String formatLastNameToUpperCase()
    @Test
    public void testformatLastNameToUpperCase() {
        Assert.assertEquals("LANCIENNE", this.anneMarie.formatLastNameToUpperCase());
    }

    // String formatPersonCivilitiesToString()
    @Test
    public void testFormatPersonCivilitiesToString() {
        Assert.assertEquals("MME Anne-Marie LANCIENNE", this.anneMarie.formatPersonCivilitiesToString());
        Assert.assertEquals("MME Anna MONFORT", this.anna.formatPersonCivilitiesToString());
    }

    // String formatClientToString()
    @Test
    public void testFormatClientToString() {
        Assert.assertEquals("MME Anne-Marie LANCIENNE", this.anneMarie.formatClientToString());
    }

    // AddressExpeption of new InternetAddress()
    @Test(expected = AddressException.class)
    public void whenAddressExceptionIsThrown_thenExpectationSatisfied() throws AddressException {
        anneMarie.setEmail(new InternetAddress("a@"));
    }

}
