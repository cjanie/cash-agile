package fr.cjanienumerique.mobile.android.cashagile.entities;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import fr.cjanienumerique.mobile.android.cashagile.converters.ClientConverter;
import fr.cjanienumerique.mobile.android.cashagile.enums.Gender;

public class ClientTest {

    private Client clientPerson;

    private Client clientCompany;

    @Before
    public void setUp() throws AddressException {
        this.clientPerson = new Client<Person>();
        Person person = new Person(Gender.MME, "ANNA", "Montfort");
        person.setEmail(new InternetAddress("a@nna"));
        this.clientPerson.setClient(person);
        this.clientCompany = new Client<Company>();
        Company company = new Company("BAT", "889");
        company.setEmail(new InternetAddress("a@bat"));
        this.clientCompany.setClient(company);
    }

    // String serialize()
    @Test
    public void testSerialize() {
        String jsonPerson = "{\"client\":{\"client_person\":\"{\\\"gender\\\":\\\"MME\\\",\\\"firstName\\\":\\\"ANNA\\\",\\\"lastName\\\":\\\"Montfort\\\",\\\"email\\\":{\\\"address\\\":\\\"a@nna\\\"}}\"}}";
        Assert.assertEquals(jsonPerson, clientPerson.serialize());
        Client deserializedPerson = ClientConverter.jsonToClient(clientPerson.serialize());
        Assert.assertEquals("ANNA", ((Person) deserializedPerson.getClient()).getFirstName());
        String jsonCompany = "{\"client\":{\"client_company\":\"{\\\"name\\\":\\\"BAT\\\",\\\"reference\\\":\\\"889\\\",\\\"email\\\":{\\\"address\\\":\\\"a@bat\\\"}}\"}}";
        Assert.assertEquals(jsonCompany, this.clientCompany.serialize());
        Client deserializedCompany = ClientConverter.jsonToClient(clientCompany.serialize());
        Assert.assertEquals("BAT", ((Company) deserializedCompany.getClient()).getName());
    }
}
