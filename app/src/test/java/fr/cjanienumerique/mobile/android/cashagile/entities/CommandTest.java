package fr.cjanienumerique.mobile.android.cashagile.entities;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.cjanienumerique.mobile.android.cashagile.exceptions.InvalidPriceException;
import fr.cjanienumerique.mobile.android.cashagile.exceptions.InvalidQuantityException;


public class CommandTest {

    private Command command;

    @Before
    public void setUp() throws InvalidQuantityException, InvalidPriceException {
        Item item = new Item ("espresso", 1.5F);
        this.command = new Command(10, item);

    }

    @Test
    public void testGetCommandTotal() {
        Assert.assertEquals(15F, command.getTotalCommand(), 0);
    }

    @Test(expected = InvalidQuantityException.class)
    public void whenNegativeNumberExceptionIsThrown_thenExpectationSatisfied() throws InvalidQuantityException {
        this.command.setQuantity(-10);
    }

    @Test(expected = InvalidQuantityException.class)
    public void testTheConstructor() throws InvalidQuantityException, InvalidPriceException {
        this.command = new Command(-1, new Item("Blé", 1.6F));
    }

}
