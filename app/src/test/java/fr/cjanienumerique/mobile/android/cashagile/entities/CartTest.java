package fr.cjanienumerique.mobile.android.cashagile.entities;

import android.app.Application;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import fr.cjanienumerique.mobile.android.cashagile.entities.Cart;
import fr.cjanienumerique.mobile.android.cashagile.entities.Command;
import fr.cjanienumerique.mobile.android.cashagile.entities.Item;
import fr.cjanienumerique.mobile.android.cashagile.exceptions.InvalidPriceException;
import fr.cjanienumerique.mobile.android.cashagile.exceptions.InvalidQuantityException;

public class CartTest {

    private Cart cart;

    private Command espresso;

    private Command bier;

    private Command soap;


    @Before
    public void setUp() throws InvalidQuantityException, InvalidPriceException {
        this.cart = new Cart();
        this.espresso = new Command(2, new Item("espresso", 1.5F));
        this.bier = new Command(2, new Item("bier", 3F));
        this.soap = new Command(2, new Item("soap", 1.5F));
    }

    // String formatCartToString()
    @Test
    public void testFormatCartToString() throws InvalidQuantityException {

        Assert.assertEquals("0 commande", this.cart.formatCartToString());

        this.cart.getCommands().add(this.espresso);
        Assert.assertEquals("1 commande", this.cart.formatCartToString());

        this.cart.getCommands().add(this.bier);
        Assert.assertEquals("2 commandes", this.cart.formatCartToString());
    }

    // Float getTotal()
    @Test
    public void testGetTotal() {
        this.cart.getCommands().add(this.espresso);
        this.cart.getCommands().add(this.bier);
        Assert.assertEquals(9F, this.cart.getTotal(), 0);
    }

    // void addCommand(Command command)
    @Test
    public void testAddCommand() {
        this.cart.addCommand(this.espresso);
        this.cart.addCommand(this.bier);
        this.cart.addCommand(this.soap);
        Assert.assertEquals(3, cart.getCommands().size());
    }

    // void updateCommand(int index, Command command)
    @Test
    public void testUpdateCommand() throws InvalidQuantityException {
        this.cart.addCommand(this.espresso);
        this.cart.addCommand(this.bier);
        this.cart.updateCommand(1, soap);
        Assert.assertEquals(2, cart.getCommands().size());
        Assert.assertEquals("soap", cart.getCommands().get(1).getItem().getName());
    }

    // void deleteCommand(int index)
    @Test
    public void testDeleteCommand() {
        this.cart.addCommand(this.espresso);
        this.cart.addCommand(this.bier);
        this.cart.addCommand(this.soap);
        Assert.assertEquals(3, cart.getCommands().size());
        cart.deleteCommand(1);
        Assert.assertEquals(2, cart.getCommands().size());
        Assert.assertEquals("soap", cart.getCommands().get(1).getItem().getName());
        cart.deleteCommand(-1);
        Assert.assertEquals(2, cart.getCommands().size());
        cart.deleteCommand(8);
        Assert.assertEquals(2, cart.getCommands().size());
    }

    // Command getCommand(int index)
    @Test
    public void testGetCommand() {
        this.cart.addCommand(this.espresso);
        this.cart.addCommand(this.bier);
        Assert.assertEquals("bier", cart.getCommand(1).getItem().getName());
        Assert.assertEquals(null, cart.getCommand(5));
        Assert.assertEquals(null, cart.getCommand(-5));
    }


    // functions to print the cart grid
    @Test
    public void testFormatQuantityToStringContentOfGrid() throws InvalidQuantityException, InvalidPriceException {
        // create an instance of Cart to test it
        Item espresso = new Item("espresso", 1.5F);
        Item bier = new Item("bier", 3F);
        Command command = new Command(2, espresso);
        Command command2 = new Command(20, bier);
        List<Command> commands = new ArrayList<>();
        commands.add(command);
        commands.add(command2);
        Cart cart = new Cart();
        cart.setCommands(commands);
        Assert.assertEquals(2, cart.formatQuantitiesToStringContentOfGrid().get(0).length());
        Assert.assertEquals(2, cart.formatQuantitiesToStringContentOfGrid().get(1).length());
        Assert.assertEquals("20", cart.formatQuantitiesToStringContentOfGrid().get(1).toString());
        Assert.assertEquals(" 2", cart.formatQuantitiesToStringContentOfGrid().get(0).toString());

    }

    @Test
    public void testFormatItemNameToStringContentOfGrid() throws InvalidQuantityException, InvalidPriceException {
        // create an instance of Cart to test it
        Item espresso = new Item("espresso", 1.5F);
        Item bier = new Item("bier", 3F);
        Command command = new Command(2, espresso);
        Command command2 = new Command(20, bier);
        List<Command> commands = new ArrayList<>();
        commands.add(command);
        commands.add(command2);
        Cart cart = new Cart();
        cart.setCommands(commands);
        Assert.assertEquals(8, cart.formatItemNamesToStringContentOfGrid().get(0).length());
        Assert.assertEquals(8, cart.formatItemNamesToStringContentOfGrid().get(1).length());
    }

    @Test
    public void testFormatUnityPricesToStringContentOfGrid() throws InvalidQuantityException, InvalidPriceException {
        // create an instance of Cart to test it
        Item espresso = new Item("espresso", 1.5F);
        Item bier = new Item("bier", 3F);
        Command command = new Command(2, espresso);
        Command command2 = new Command(20, bier);
        List<Command> commands = new ArrayList<>();
        commands.add(command);
        commands.add(command2);
        Cart cart = new Cart();
        cart.setCommands(commands);
        Assert.assertEquals(3, cart.formatUnityPricesToStringContentOfGrid().get(0).length());
    }

    @Test
    public void testprintGrid() throws InvalidQuantityException, InvalidPriceException {
        // create an instance of Cart to test it
        Item espresso = new Item("espresso", 10.55F);
        Item bier = new Item("bier", 3F);
        Command command = new Command(2, espresso);
        Command command2 = new Command(20, bier);
        List<Command> commands = new ArrayList<>();
        commands.add(command);
        commands.add(command2);
        Cart cart = new Cart();
        cart.setCommands(commands);
        List<String[]> grid = cart.printGrid();
        Assert.assertEquals("bier    ", grid.get(1)[1]);
        Assert.assertEquals("10.55", grid.get(0)[2]);
        Assert.assertEquals(" 3.00", grid.get(1)[2]);

    }
}
