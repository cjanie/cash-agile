package fr.cjanienumerique.mobile.android.cashagile.core;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ListOfFloatsTest {

    @Test
    public void testGetSplitsBeforePoint() {
        List<Float> floats = new ArrayList<>();
        floats.add(1F);
        floats.add(2.55F);
        floats.add(20.7F);
        Assert.assertEquals("20", new ListOfFloats(floats).getSplitsBeforePoint().get(2));
    }

    @Test
    public void testGetSplitsAfterPoint() {
        List<Float> floats = new ArrayList<>();
        floats.add(1F);
        floats.add(2.55F);
        floats.add(20.7F);
        Assert.assertEquals("7", new ListOfFloats(floats).getSplitsAfterPoint().get(2));
    }

    @Test
    public void testGetSplitsBeforePointColumnWidth() {
        List<Float> floats = new ArrayList<>();
        floats.add(1F);
        floats.add(2.55F);
        floats.add(20.7F);
        Assert.assertEquals(2, new ListOfFloats(floats).getSplitsBeforePointColumnWidth());
    }

    @Test
    public void testGetSplitsAfterPointColumnWidth() {
        List<Float> floats = new ArrayList<>();
        floats.add(1F);
        floats.add(2.550F);
        floats.add(20.7F);
        Assert.assertEquals(2, new ListOfFloats(floats).getSplitsAfterPointColumnWidth());
    }

    @Test
    public void testGetIndexOfFloatPoints() {
        List<Float> floats = new ArrayList<>();
        floats.add(1F);
        floats.add(2.550F);
        floats.add(20.7F);
        Assert.assertEquals(1, new ListOfFloats(floats).getIndexOfFloatPoints().get(0), 0);
        Assert.assertEquals(1, new ListOfFloats(floats).getIndexOfFloatPoints().get(1), 0);
        Assert.assertEquals(2, new ListOfFloats(floats).getIndexOfFloatPoints().get(2), 0);
    }

    @Test
    public void testFormatSplitsBeforePointToIntegers() {
        List<Float> floats = new ArrayList<>();
        floats.add(1F);
        floats.add(2.550F);
        floats.add(20.7F);
        Assert.assertEquals(2, new ListOfFloats(floats).formatSplitsBeforePointToIntegers().get(1), 0);
    }

    @Test
    public void testFormatSplitBeforePointToContentOfGrid() {
        List<Float> floats = new ArrayList<>();
        floats.add(1F);
        floats.add(2.550F);
        floats.add(20.7F);
        Assert.assertEquals(" 1", new ListOfFloats(floats).formatSplitsBeforePointToContentOfGrid().get(0));
        Assert.assertEquals(" 2", new ListOfFloats(floats).formatSplitsBeforePointToContentOfGrid().get(1));
        Assert.assertEquals("20", new ListOfFloats(floats).formatSplitsBeforePointToContentOfGrid().get(2));
    }

    @Test
    public void testFormatSplitsAfterPointToContentOfGrid() {
        List<Float> floats = new ArrayList<>();
        floats.add(1F);
        floats.add(2.550F);
        floats.add(20.7F);
        Assert.assertEquals("00", new ListOfFloats(floats).formatSplitsAfterPointToContentOfGrid().get(0));
        Assert.assertEquals("55", new ListOfFloats(floats).formatSplitsAfterPointToContentOfGrid().get(1));
        Assert.assertEquals("70", new ListOfFloats(floats).formatSplitsAfterPointToContentOfGrid().get(2));
    }

    @Test
    public void testFormatFloatsToContentOfGrid() {
        List<Float> floats = new ArrayList<>();
        floats.add(1F);
        floats.add(2.550F);
        floats.add(20.7F);
        Assert.assertEquals(" 1.00", new ListOfFloats(floats).formatFloatsToContentOfGrid().get(0));
        Assert.assertEquals(" 2.55", new ListOfFloats(floats).formatFloatsToContentOfGrid().get(1));
    }

}
