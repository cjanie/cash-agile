package fr.cjanienumerique.mobile.android.cashagile.entities;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import fr.cjanienumerique.mobile.android.cashagile.converters.QuoteConverter;
import fr.cjanienumerique.mobile.android.cashagile.enums.Gender;
import fr.cjanienumerique.mobile.android.cashagile.exceptions.InvalidPriceException;
import fr.cjanienumerique.mobile.android.cashagile.exceptions.InvalidQuantityException;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.ClientInterface;

public class QuoteTest {

    // Quote to test

    private Quote quote;

    // instanciate
    @Before
    public void setUp() throws InvalidQuantityException, InvalidPriceException {
        Company company = new Company("bateau Co", "8888765978");
        ClientInterface person = new Person(Gender.MME, "anna", "Monfort");
        // create an instance of Cart
        Item espresso = new Item("espresso", 1.50F);
        Item bier = new Item("bier", 3F);
        Command command = new Command(2, espresso);
        Command command2 = new Command(20, bier);
        List<Command> commands = new ArrayList<>();
        commands.add(command);
        commands.add(command2);
        Cart cart = new Cart();
        cart.setCommands(commands);
        // create an instance of Edition
        Edition edition = new Edition();
        edition.setCompany(company);
        Client client = new Client();
        client.setClient(person);
        edition.setClient(client);
        edition.setCart(cart);
        // instanciate quote
        this.quote = new Quote(edition);
        this.quote.setId(1);
    }

    @Test
    public void testSerialize() {
        String json = "{\"id\":1,\"date_of_edition\":18584,\"edition\":\"{\\\"company\\\":\\\"{\\\\\\\"name\\\\\\\":\\\\\\\"bateau Co\\\\\\\",\\\\\\\"reference\\\\\\\":\\\\\\\"8888765978\\\\\\\"}\\\",\\\"client\\\":\\\"{\\\\\\\"client\\\\\\\":{\\\\\\\"client_person\\\\\\\":\\\\\\\"{\\\\\\\\\\\\\\\"gender\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"MME\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"firstName\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"anna\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"lastName\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"Monfort\\\\\\\\\\\\\\\"}\\\\\\\"}}\\\",\\\"cart\\\":\\\"{\\\\\\\"commands\\\\\\\":[{\\\\\\\"quantity\\\\\\\":2,\\\\\\\"item\\\\\\\":{\\\\\\\"name\\\\\\\":\\\\\\\"espresso\\\\\\\",\\\\\\\"price\\\\\\\":1.5}},{\\\\\\\"quantity\\\\\\\":20,\\\\\\\"item\\\\\\\":{\\\\\\\"name\\\\\\\":\\\\\\\"bier\\\\\\\",\\\\\\\"price\\\\\\\":3.0}}]}\\\"}\",\"sent\":false,\"purchase_order\":false}";
        Assert.assertEquals(json, quote.serialize());
        Quote deserialized = QuoteConverter.jsonToQuote(quote.serialize());
        Assert.assertEquals("MME Anna MONFORT", ((ClientInterface) deserialized.getEdition().getClient().getClient()).formatClientToString());

    }

    @Test
    public void testPrintAsString() {
        Assert.assertNotNull(quote.printAsString());
    }
}
