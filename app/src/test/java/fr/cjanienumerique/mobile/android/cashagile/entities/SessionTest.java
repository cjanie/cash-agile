package fr.cjanienumerique.mobile.android.cashagile.entities;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import fr.cjanienumerique.mobile.android.cashagile.converters.SessionConverter;

public class SessionTest {

    private Session session;

    @Before
    public void setUp() throws AddressException {
        this.session = new Session();
        Company company = new Company("BAT", "887");
        company.setEmail(new InternetAddress("a@bat"));
        this.session.setSession(company);
        this.session.setEmailPassword("passage");
    }

    // String serialize()
    @Test
    public void testSerialize() {
        String json = "{\"session\":{\"name\":\"BAT\",\"reference\":\"887\",\"email\":{\"address\":\"a@bat\"}},\"emailPassword\":\"passage\",\"quotes\":[]}";
        Assert.assertEquals(json, this.session.serialize());
        Session deserialized = SessionConverter.jsonToSession(this.session.serialize());
        Assert.assertEquals("a@bat", deserialized.getSession().getSessionEmail().getAddress());
        this.session.getSession().setSessionEmail(null);
        Assert.assertNotNull(this.session.serialize());
        Assert.assertNotEquals("", this.session.serialize());
        String json2 = "{\"session\":{\"name\":\"BAT\",\"reference\":\"887\"},\"emailPassword\":\"passage\",\"quotes\":[]}";
        Assert.assertEquals(json2, session.serialize());
    }


}
