package fr.cjanienumerique.mobile.android.cashagile.entities;

import org.junit.Before;
import org.junit.Test;

import fr.cjanienumerique.mobile.android.cashagile.exceptions.InvalidPriceException;

public class ItemTest {

    private Item item;

    @Before
    public void setUp() throws InvalidPriceException {
        this.item = new Item("Smartphone", 200F);
    }

    @Test(expected = InvalidPriceException.class)
    public void whenInvalidPriceExceptionIsThrown_thenExpectationSatisfied() throws InvalidPriceException {
        this.item.setPrice(-7F);
    }

    @Test(expected = InvalidPriceException.class)
    public void testConstructor() throws InvalidPriceException {
        this.item = new Item("fag", -2F);
    }
}
