package fr.cjanienumerique.mobile.android.cashagile.entities;


import java.util.ArrayList;

import java.util.List;

import fr.cjanienumerique.mobile.android.cashagile.core.ListOfFloats;
import fr.cjanienumerique.mobile.android.cashagile.core.ListOfIntegers;
import fr.cjanienumerique.mobile.android.cashagile.core.ListOfStrings;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.Editable;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.PrintableGrid;

public class Cart implements Editable, PrintableGrid {

    private List<Command> commands;

    public Cart() {
        this.commands = new ArrayList<>();
    }

    public List<Command> getCommands() {
        return commands;
    }

    public void setCommands(List<Command> commands) {
        this.commands = commands;
    }

    // functions

    public String formatCartToString() {
        StringBuilder stringBuilder = new StringBuilder(this.getCommands().size() + " commande"); // TODO externalize string
        if(this.getCommands().size() > 1) {
            stringBuilder.append("s");
        }
        return stringBuilder.toString();
    }

    public Float getTotal() {
        Float total = 0F;
        if(!this.commands.isEmpty()) {
            for(int i=0 ; i< this.commands.size(); i++) {
                total += this.commands.get(i).getTotalCommand();
            }
        }
        return total;
    }

    public int getNumberOfItems() {
        List<Command> commands = this.getCommands();
        int numberOfItems = 0;
        for(Command command: commands) {
            numberOfItems += command.getQuantity();
        }
        return numberOfItems;
    }

    public void addCommand(Command command) {
        this.commands.add(command);
    }

    public void updateCommand(int index, Command command) {
        if(!this.commands.isEmpty()) {
            if(index >= 0 && index < this.commands.size()) {
                this.commands.set(index, command);
            }
        }
    }

    public void deleteCommand(int index) {
        if(!this.commands.isEmpty()) {
            if(index >= 0 && index < this.commands.size()) {
                this.commands.remove(index);
            }
        }
    }

    public Command getCommand(int index) {
        Command command = null;
        if(!this.commands.isEmpty()) {
            if(index >= 0 && index < this.commands.size()) {
                command = this.commands.get(index);
            }
        }
        return command;
    }

    // prepare the columns to implement PrintableGrid

    // For item names column, list the item names and use the ListOfString functions;

    public List<String> getItemNames() {
        List<String> itemNames = new ArrayList<>();
        for(Command command: this.commands) {
            itemNames.add(command.getItem().getName());
        }
        return itemNames;
    }

    public List<String> formatItemNamesToStringContentOfGrid() {
        return new ListOfStrings(this.getItemNames()).formatStringsToContentOfGrid();
    }


    // For the quantity column, list the quantities and use the ListOfInteger functions.

    public List<Integer> getQuantities() {
        List<Integer> quantities = new ArrayList<>();
        for(Command command: this.commands) {
            quantities.add(command.getQuantity());
        }
        return quantities;
    }

    public List<String> formatQuantitiesToStringContentOfGrid() {
        return new ListOfIntegers(this.getQuantities()).formatIntegersToContentOfGrid();
    }

    // For the unity price column, list the unity prices and use the ListOfFloats functions.

    public List<Float> getUnityPrices() {
        List<Float> unityPrices = new ArrayList<>();
        for(Command command: this.commands) {
            unityPrices.add(command.getItem().getPrice());
        }
        return unityPrices;
    }

    public List<String> formatUnityPricesToStringContentOfGrid() {
        return new ListOfFloats(this.getUnityPrices()).formatFloatsToContentOfGrid();
    }



    // grid

    @Override
    public List<String[]> printGrid() {
        List<String[]> grid = new ArrayList<>();
        for(int i=0; i<this.commands.size(); i++) {
            String[] commandArray = new String[] {
                    this.formatQuantitiesToStringContentOfGrid().get(i),
                    this.formatItemNamesToStringContentOfGrid().get(i),
                    this.formatUnityPricesToStringContentOfGrid().get(i)
            };
            grid.add(commandArray);
        }
        return grid;
    }
}
