package fr.cjanienumerique.mobile.android.cashagile.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.entities.Company;
import fr.cjanienumerique.mobile.android.cashagile.entities.Session;
import fr.cjanienumerique.mobile.android.cashagile.enums.ActionType;
import fr.cjanienumerique.mobile.android.cashagile.enums.SuccessType;
import fr.cjanienumerique.mobile.android.cashagile.ui.company.CompanyViewModel;

public class SessionFormFragment extends Fragment implements View.OnClickListener {

    private CompanyViewModel companyViewModel;

    private EditText companyNameView;
    private EditText companyReferenceView;
    private EditText sessionEmailView;
    private EditText sessionPasswordView;
    private TextView errorView;
    private FloatingActionButton fabValid;

    private String companyName;
    private String companyReference;
    private String sessionEmail;
    private String sessionPassword;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.companyViewModel = ViewModelProviders.of(this).get(CompanyViewModel.class);

        View root = inflater.inflate(R.layout.fragment_session_form, container, false);

        // sub fragment company form
        this.companyNameView = root.findViewById(R.id.company_name);
        this.companyReferenceView = root.findViewById(R.id.company_reference);
        // sub fragment email
        this.sessionEmailView = root.findViewById(R.id.email);
        // sub fragment password
        this.sessionPasswordView = root.findViewById(R.id.password);
        // sub fragment valid
        this.errorView = root.findViewById(R.id.error);
        this.fabValid = root.findViewById(R.id.fab_valid);

        this.fabValid.setOnClickListener(this);

        try {
            this.companyViewModel.getSession().observe(this, new Observer<Session>() {
                @Override
                public void onChanged(Session session) {
                    if(session != null) {
                        Company company = session.getSession();
                        if(company != null) {
                            companyName = company.getName();
                            companyReference = company.getReference();
                            sessionEmail = company.getEmail().getAddress();
                        }
                        sessionPassword = session.getEmailPassword();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (AddressException e) {
            e.printStackTrace();
        }


        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.sessionEmailView.setHint(this.getContext().getString(R.string.gmail_hint));
        this.companyNameView.setText(this.companyName);
        this.companyReferenceView.setText(this.companyReference);
        this.sessionEmailView.setText(this.sessionEmail);
        this.sessionPasswordView.setText(this.sessionPassword);
    }

    @Override
    public void onClick(View v) {
        if(TextUtils.isEmpty(this.companyNameView.getText())) {
            this.errorView.setText(getContext().getString(R.string.error_name_company));
        } else if(TextUtils.isEmpty(this.companyReferenceView.getText())) {
            errorView.setText(getContext().getString(R.string.error_reference));
        } else if(TextUtils.isEmpty(sessionEmailView.getText())) {
            errorView.setText(R.string.error_email);
        } else if(TextUtils.isEmpty(sessionPasswordView.getText())) {
            errorView.setText(R.string.error_password);
        } else {
            errorView.setText("");
            Company company = new Company(companyNameView.getText().toString(), companyReferenceView.getText().toString());
            // SET EMAIL
            try {
                InternetAddress email = new InternetAddress(sessionEmailView.getText().toString());
                company.setEmail(email);
                this.companyViewModel.setCompanyToEdition(company);
                this.companyViewModel.saveSession(company, sessionPasswordView.getText().toString());

                SuccessFragment successFragment = new SuccessFragment();
                successFragment.setSuccessType(SuccessType.COMPANY);
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frame_of_company_fragment, successFragment)
                        .commit();

            } catch (AddressException e) {
                errorView.setText(getContext().getString(R.string.error_email_invalid));
            } catch (IOException e) {
                errorView.setText(e.getMessage());
            }
        }
    }
}
