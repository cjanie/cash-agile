package fr.cjanienumerique.mobile.android.cashagile.ui;

import android.app.Application;
import android.os.Build;
import android.view.View;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.AndroidViewModel;

import androidx.lifecycle.MutableLiveData;

import java.io.IOException;


import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.entities.Client;
import fr.cjanienumerique.mobile.android.cashagile.entities.Document;
import fr.cjanienumerique.mobile.android.cashagile.entities.Quote;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.ClientInterface;
import fr.cjanienumerique.mobile.android.cashagile.managers.FileStorageManagerSession;
import fr.cjanienumerique.mobile.android.cashagile.managers.PdfMaker;
import fr.cjanienumerique.mobile.android.cashagile.managers.PdfStorage;
import fr.cjanienumerique.mobile.android.cashagile.repositories.QuoteRepository;
import fr.cjanienumerique.mobile.android.cashagile.services.ConfigurationService;
import fr.cjanienumerique.mobile.android.cashagile.services.GmailAsyncSender;


public class DocumentViewModel extends AndroidViewModel {

    private QuoteRepository quoteRepository;

    private FileStorageManagerSession fileStorageManager;

    private ConfigurationService configurationService;


    public DocumentViewModel(Application application) throws IOException, AddressException {
        super(application);
        this.quoteRepository = QuoteRepository.getInstance();
        this.fileStorageManager = FileStorageManagerSession.getInstance(application);
        this.configurationService = ConfigurationService.getInstance();
    }

    public MutableLiveData<Quote> getLastQuote() throws IOException, AddressException {
        return this.quoteRepository.getLastQuote(this.fileStorageManager);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void makePdf(View view) throws IOException {
        PdfMaker pdfMaker = PdfMaker.getInstance();
        pdfMaker.makePdf(view, PdfStorage.getInstance(this.getApplication()));
    }






    public void sendEmail(Document document) throws IOException, MessagingException {
        if(this.getClientEmail(document) != null) {
            String subject = "";
            if(document instanceof Quote) {
                subject = this.getApplication().getApplicationContext().getString(R.string.quote);
            } else {
                subject = this.getApplication().getApplicationContext().getString(R.string.document);
            }
            String body = document.printAsString();
            InternetAddress emailSender = this.getSessionEmail();
            String emailSenderPassword = this.getSessionEmailPassword();
            InternetAddress emailRecipient = this.getClientEmail(document);

            GmailAsyncSender gmailAsyncSender = new GmailAsyncSender(emailSender.getAddress(), emailSenderPassword);
            gmailAsyncSender.setRecipient(emailRecipient);
            gmailAsyncSender.setSubject(subject);
            gmailAsyncSender.setBody(body);
            gmailAsyncSender.execute();


        }

    }


    private InternetAddress getSessionEmail() throws IOException, AddressException {
        return this.configurationService.getEmail(this.fileStorageManager);
    }

    private String getSessionEmailPassword() throws IOException, AddressException {
        return this.configurationService.getPassword(this.fileStorageManager);
    }

    private InternetAddress getClientEmail(Document document) {
        InternetAddress clientEmail = null;
        if(document != null) {
            Client<ClientInterface> client = document.getEdition().getClient();
            clientEmail = client.getClient().getClientEmail();
        }
        return clientEmail;
    }

}
