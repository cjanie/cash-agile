package fr.cjanienumerique.mobile.android.cashagile.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;

import javax.mail.internet.AddressException;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.entities.Client;
import fr.cjanienumerique.mobile.android.cashagile.entities.Company;
import fr.cjanienumerique.mobile.android.cashagile.entities.Session;
import fr.cjanienumerique.mobile.android.cashagile.enums.ActionType;
import fr.cjanienumerique.mobile.android.cashagile.enums.SuccessType;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.ClientInterface;
import fr.cjanienumerique.mobile.android.cashagile.ui.cart.CartFragment;
import fr.cjanienumerique.mobile.android.cashagile.ui.cart.CartViewModel;
import fr.cjanienumerique.mobile.android.cashagile.ui.client.ClientFragment;
import fr.cjanienumerique.mobile.android.cashagile.ui.client.ClientViewModel;
import fr.cjanienumerique.mobile.android.cashagile.ui.company.CompanyViewModel;

public class SuccessFragment extends Fragment implements View.OnClickListener {


    private TextView messageView;

    private SuccessType successType; // set from client form fragment OR company form fragment

    private String message; // set from onCreateView()


    public void setSuccessType(SuccessType successType) {
        this.successType = successType;
    }

    private FloatingActionButton buttonModify;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if(this.successType != null) {
            if(this.successType.equals(SuccessType.CART)) {
                CartViewModel cartViewModel = ViewModelProviders.of(this).get(CartViewModel.class);
            } else if(this.successType.equals(SuccessType.CLIENT)) {
                ClientViewModel clientViewModel = ViewModelProviders.of(this).get(ClientViewModel.class);
                clientViewModel.getClient().observe(this, new Observer<Client>() {
                    @Override
                    public void onChanged(Client client) {
                        if(client != null) {
                            message = clientViewModel.buildSuccessMessage(client);
                        }
                    }
                });

            } else if(this.successType.equals(SuccessType.COMPANY)) {
                CompanyViewModel companyViewModel = ViewModelProviders.of(this).get(CompanyViewModel.class);
                try {
                    companyViewModel.getSession().observe(this, new Observer<Session>() {
                        @Override
                        public void onChanged(Session session) {
                            if(session != null) {
                                Company company = session.getSession();
                                if(company != null) {
                                    message = companyViewModel.buildSuccessMessage(company);
                                }
                            }
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (AddressException e) {
                    e.printStackTrace();
                }
            }
        }


        View root = inflater.inflate(R.layout.fragment_success, container, false);
        this.messageView = root.findViewById(R.id.success_message);
        this.buttonModify = root.findViewById(R.id.fab_success_modify);

        buttonModify.setOnClickListener(this);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.messageView.setText(this.message);


    }


    @Override
    public void onClick(View v) {
        if(this.successType != null) {
            if(this.successType.equals(SuccessType.CLIENT)) {
                ClientFormFragment clientFormFragment = new ClientFormFragment();
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frame_of_client_fragment, clientFormFragment)
                        .commit();

            } else if(this.successType.equals(SuccessType.COMPANY)) {
                SessionFormFragment sessionFormFragment = new SessionFormFragment();
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frame_of_company_fragment, sessionFormFragment)
                        .commit();
                System.out.println("test the modify click from company success");
            }
        }
    }
}
