package fr.cjanienumerique.mobile.android.cashagile.interfaces;

// Cf. json tutorial: http://www.studytrails.com/java/json/java-google-json-introduction/
public interface Convertible {

    String serialize();

}
