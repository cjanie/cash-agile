package fr.cjanienumerique.mobile.android.cashagile.enums;

public enum FileTarget {
    SAMPLE, SAVEABLE, SESSION, QUOTE
}
