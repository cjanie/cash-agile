package fr.cjanienumerique.mobile.android.cashagile.ui.edition;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.entities.Document;
import fr.cjanienumerique.mobile.android.cashagile.entities.Edition;
import fr.cjanienumerique.mobile.android.cashagile.entities.Quote;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.ClientInterface;
import fr.cjanienumerique.mobile.android.cashagile.ui.DocumentViewModel;


public class EditionFragment extends Fragment {

    private DocumentViewModel documentViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.documentViewModel = ViewModelProviders.of(this).get(DocumentViewModel.class);

        View root = inflater.inflate(R.layout.fragment_edition, container);

        final TextView viewCart = root.findViewById(R.id.cart);
        final TextView viewClient = root.findViewById(R.id.client);
        final TextView viewCompany = root.findViewById(R.id.company);
/*
        try {
            documentViewModel.getLastQuote().observe(this, new Observer<Quote>() {
                @Override
                public void onChanged(Quote quote) {
                    if(quote != null) {
                        viewCompany.setText(getContext().getString(R.string.from)+ " " + quote.getEdition().getCompany().getName());
                        viewClient.setText(getContext().getString(R.string.to) + " " + ((ClientInterface) quote.getEdition().getClient().getClient()).formatClientToString());
                        viewCart.setText(quote.getEdition().getCart().formatCartToString());
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

 */

        return root;

    }
}
