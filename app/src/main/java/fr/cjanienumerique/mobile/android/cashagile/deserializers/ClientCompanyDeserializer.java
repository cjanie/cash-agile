package fr.cjanienumerique.mobile.android.cashagile.deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import fr.cjanienumerique.mobile.android.cashagile.converters.CompanyConverter;
import fr.cjanienumerique.mobile.android.cashagile.entities.Company;

public class ClientCompanyDeserializer implements JsonDeserializer<Company> {

    @Override
    public Company deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if(json == null) {
            return null;
        } else {
            String jsonClientCompany = json.getAsJsonObject().get("client_company").getAsString();
            return CompanyConverter.jsonToCompany(jsonClientCompany);
        }
    }

}
