package fr.cjanienumerique.mobile.android.cashagile.interfaces;

import java.util.HashMap;

import javax.mail.internet.AddressException;

public interface Loadable {

    void load(HashMap<String, String> data) throws AddressException;
}
