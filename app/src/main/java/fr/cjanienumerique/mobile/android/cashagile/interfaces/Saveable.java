package fr.cjanienumerique.mobile.android.cashagile.interfaces;

import java.util.HashMap;

public interface Saveable {

    String formatSaveableToString();

    HashMap<String, String> formatSaveableToHashMap();

    String[] getStructure();
}
