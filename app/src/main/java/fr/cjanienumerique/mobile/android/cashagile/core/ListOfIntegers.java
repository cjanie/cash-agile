package fr.cjanienumerique.mobile.android.cashagile.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListOfIntegers {
    private List<Integer> integers;

    public ListOfIntegers(List<Integer> integers) {
        this.integers = integers;
    }

    public int getIntegersColumnWidth() {
        // get the width of each integer and save it into a list of integers
        List<Integer> widths = new ArrayList<>();
        for(Integer integer: this.integers) {
            widths.add(integer.toString().length());
        }
        // sort widths to return max witdh
        Collections.sort(widths);
        return widths.get(widths.size() - 1); // max width
    }

    public List<String> formatIntegersToContentOfGrid() {
        List<String> formatedIntegers = new ArrayList<>();

        for(Integer integer: this.integers) {
            StringBuilder contentStringBuilder = new StringBuilder(integer.toString());
            while(contentStringBuilder.length() < this.getIntegersColumnWidth()) {
                contentStringBuilder.append(" ");
            }
            String blank = contentStringBuilder.substring(integer.toString().length());
            String notBlank = contentStringBuilder.substring(0, integer.toString().length());
            String formatedQuantity = blank + notBlank;
            formatedIntegers.add(formatedQuantity);
        }
        return formatedIntegers;
    }


}
