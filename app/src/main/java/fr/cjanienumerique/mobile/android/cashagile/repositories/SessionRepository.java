package fr.cjanienumerique.mobile.android.cashagile.repositories;



import androidx.lifecycle.MutableLiveData;

import java.io.IOException;

import javax.mail.internet.AddressException;


import fr.cjanienumerique.mobile.android.cashagile.datalayers.DataLayerSession;
import fr.cjanienumerique.mobile.android.cashagile.entities.Session;
import fr.cjanienumerique.mobile.android.cashagile.managers.FileStorageManagerSession;

public class SessionRepository {

    private DataLayerSession dataLayerSession;

    private static SessionRepository INSTANCE;

    private SessionRepository() {
        this.dataLayerSession = DataLayerSession.getInstance();
    }

    public static SessionRepository getInstance() {
        if(SessionRepository.INSTANCE == null) {
            SessionRepository.INSTANCE = new SessionRepository();
        }
        return SessionRepository.INSTANCE;
    }

    public void saveSession(Session session, FileStorageManagerSession fileStorageManager) throws IOException, AddressException {
        this.dataLayerSession.saveSession(session, fileStorageManager);
    }

    public MutableLiveData<Session> getSession(FileStorageManagerSession fileStorageManager) throws IOException, AddressException {
        return this.dataLayerSession.getSession(fileStorageManager);
    }

}
