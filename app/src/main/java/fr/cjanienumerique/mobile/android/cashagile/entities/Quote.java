package fr.cjanienumerique.mobile.android.cashagile.entities;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.room.Entity;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import fr.cjanienumerique.mobile.android.cashagile.converters.EditionConverter;
import fr.cjanienumerique.mobile.android.cashagile.converters.LocalDateConverter;
import fr.cjanienumerique.mobile.android.cashagile.converters.QuoteConverter;
import fr.cjanienumerique.mobile.android.cashagile.enums.DocumentType;
import fr.cjanienumerique.mobile.android.cashagile.managers.FileWriterManager;


public class Quote extends Document {

    private Boolean purchaseOrder;

    // Constructor
    @RequiresApi(api = Build.VERSION_CODES.O)
    public Quote(Edition edition) {
        super(edition);
        this.title = DocumentType.QUOTE.toString();
        this.purchaseOrder = false;
    }


    public Boolean getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(Boolean purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    // to implement Printable

    @RequiresApi(api = Build.VERSION_CODES.O)
    private List<String> printAsList() {
        // each line of the quote is a String in an Array of Strings
        List<String> lines = new ArrayList<>();
        if(this.id != null) {

            if(this.getEdition() != null) {
                if(this.getEdition().getCompany() != null) {
                    if(this.getEdition().getCompany().getName() != null) {
                        lines.add(this.getEdition().getCompany().getName().toUpperCase());
                    }
                    if(this.getEdition().getCompany().getReference() != null) {
                        lines.add(this.getEdition().getCompany().getReference());
                    }
                }
                if(this.getEdition().getClient() != null) {
                    if(this.getEdition().getClient().getClient() != null) {
                        if(this.getEdition().getClient().getClient() instanceof Person) {
                            lines.add(((Person) this.getEdition().getClient().getClient()).formatPersonCivilitiesToString());
                        } else if(this.getEdition().getClient().getClient() instanceof Company) {
                            lines.add(((Company) this.getEdition().getClient().getClient()).getName());
                        }
                    }

                }
            }
            String date = this.getDateOfEdition().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT));
            lines.add(date);
            String title = DocumentType.QUOTE.toString() + " n° " + this.id.toString();
            lines.add(title);

            // TODO label
            // cart as a list of strings ; each command as a String
            if(this.getEdition().getCart() != null) {
                List<String[]> cart = this.getEdition().getCart().printGrid();

                for (String [] commandArray : cart) {
                    StringBuilder command = new StringBuilder();
                    for (int i=0; i<commandArray.length; i++) {
                        command.append(commandArray[i]);
                        command.append(" | ");
                    }
                    lines.add(command.toString());
                    command.append("\n");
                }
                // total
                String total = "Total €" + ": " + this.getEdition().getCart().getTotal();
                lines.add(total);
            }
        }
        for(String s: lines) {
            System.out.println(s);
        }
        return lines;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public String printAsString() {
        StringBuilder stringBuilder = new StringBuilder();
        List<String> lines = this.printAsList();
        for(String line: lines) {
            stringBuilder.append(line);
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }


    @Override
    public String serialize() {
        return QuoteConverter.quoteToJson(this);
    }
}
