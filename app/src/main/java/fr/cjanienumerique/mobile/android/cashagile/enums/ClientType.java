package fr.cjanienumerique.mobile.android.cashagile.enums;

public enum ClientType {

    M, MME, COMPANY
}
