package fr.cjanienumerique.mobile.android.cashagile.managers;

import android.app.Application;

import fr.cjanienumerique.mobile.android.cashagile.enums.FileTarget;



public class FileStorageManagerSession extends FileStorageManager {

    private static FileStorageManagerSession INSTANCE;

    private FileStorageManagerSession(Application application) {
        super(application);
        this.fileTarget = FileTarget.SESSION;
    }

    public static FileStorageManagerSession getInstance(Application application) {
        if(FileStorageManagerSession.INSTANCE == null) {
            FileStorageManagerSession.INSTANCE = new FileStorageManagerSession(application);
        }
        return FileStorageManagerSession.INSTANCE;
    }

    @Override
    public String getTargetFileName() {
        return "session";
    }
}
