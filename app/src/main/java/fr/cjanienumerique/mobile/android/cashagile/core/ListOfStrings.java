package fr.cjanienumerique.mobile.android.cashagile.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListOfStrings {

    private List<String> strings;

    public ListOfStrings(List<String> strings) {
        this.strings = strings;
    }

    public int getStringsColumnWidth() {
        // get the width of each string and save it into a list of integers
        List<Integer> widths = new ArrayList<>();
        for(String string : this.strings) {
            widths.add(string.length());
        }
        // sort widths to return max width
        Collections.sort(widths);
        return widths.get(widths.size() - 1);
    }

    public List<String> formatStringsToContentOfGrid() {
        List<String> formatedStrings = new ArrayList<>();
        StringBuilder stringBuilder = null;
        for(String string: this.strings) {
            stringBuilder = new StringBuilder(string);
            while (stringBuilder.length() < this.getStringsColumnWidth()) {
                stringBuilder.append(" ");
            }
            formatedStrings.add(stringBuilder.toString());
        }
        return formatedStrings;
    }

}
