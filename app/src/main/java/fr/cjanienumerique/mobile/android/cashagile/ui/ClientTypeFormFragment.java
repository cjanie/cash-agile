package fr.cjanienumerique.mobile.android.cashagile.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.entities.Client;
import fr.cjanienumerique.mobile.android.cashagile.entities.Company;
import fr.cjanienumerique.mobile.android.cashagile.entities.Person;
import fr.cjanienumerique.mobile.android.cashagile.enums.Gender;
import fr.cjanienumerique.mobile.android.cashagile.ui.client.ClientViewModel;

public class ClientTypeFormFragment extends Fragment implements RadioGroup.OnCheckedChangeListener {

    private ClientViewModel clientViewModel;

    private RadioGroup clientTypeView;

    private int checkedId;

    private final String CHECKED_ID_SAVE = "CHECKED_ID_SAVE";

    public void refreshScreen() {
        this.clientTypeView.check(this.checkedId);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.clientViewModel = ViewModelProviders.of(this).get(ClientViewModel.class);

        View root = inflater.inflate(R.layout.fragment_client_type_form, container, false);

        this.clientTypeView = root.findViewById(R.id.client_type);

        this.clientTypeView.setOnCheckedChangeListener(this);

        this.clientViewModel.getClient().observe(this, new Observer<Client>() {
            @Override
            public void onChanged(Client client) {
                if(client != null) {
                    if(client.getClient() != null) {
                        if(client.getClient() instanceof Person) {
                            if(((Person) client.getClient()).getGender().equals(Gender.M)) {
                                checkedId = R.id.radioButton_mister;

                            } else if(((Person) client.getClient()).getGender().equals(Gender.MME)) {
                                checkedId = R.id.radioButton_madame;
                            }
                        } else if(client.getClient() instanceof Company) {
                            checkedId = R.id.radioButton_company;
                        }
                    }
                }
            }
        });

        if(savedInstanceState != null) {
            this.checkedId = Integer.parseInt(savedInstanceState.getString(CHECKED_ID_SAVE));
            this.refreshScreen(); // Doesn't
        }

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.clientTypeView.check(this.checkedId);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(this.CHECKED_ID_SAVE, String.valueOf(this.checkedId));
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
       this.checkedId = checkedId;
        if(checkedId == R.id.radioButton_mister || checkedId == R.id.radioButton_madame) {
            ClientPersonFormFragment clientPersonFormFragment = new ClientPersonFormFragment();
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_customized_form, clientPersonFormFragment)
                    .addToBackStack(null)
                    .commit();

            if(checkedId == R.id.radioButton_mister) {
                clientPersonFormFragment.setGender(Gender.M);

            } else if(checkedId == R.id.radioButton_madame) {
                clientPersonFormFragment.setGender(Gender.MME);

            }

        } else if(checkedId == R.id.radioButton_company) {
            ClientCompanyFormFragment clientCompanyFormFragment = new ClientCompanyFormFragment();
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_customized_form, clientCompanyFormFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

}
