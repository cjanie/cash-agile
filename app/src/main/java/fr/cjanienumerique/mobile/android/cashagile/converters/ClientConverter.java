package fr.cjanienumerique.mobile.android.cashagile.converters;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import fr.cjanienumerique.mobile.android.cashagile.deserializers.ClientCompanyDeserializer;
import fr.cjanienumerique.mobile.android.cashagile.deserializers.ClientPersonDeserializer;
import fr.cjanienumerique.mobile.android.cashagile.entities.Client;
import fr.cjanienumerique.mobile.android.cashagile.entities.Company;
import fr.cjanienumerique.mobile.android.cashagile.entities.Person;
import fr.cjanienumerique.mobile.android.cashagile.serializers.ClientCompanySerializer;
import fr.cjanienumerique.mobile.android.cashagile.serializers.ClientPersonSerializer;

public class ClientConverter {

    public static String clientToJson(Client client) {
        if(client == null) {
            return null;
        } else {
            String json = null;

            if(client.getClient() instanceof Person) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Person.class, new ClientPersonSerializer()).create();
                json = new String(gson.toJson(client, new TypeToken<Client<Person>>() {}.getType()));


            } else if(client.getClient() instanceof Company) {

                Gson gson = new GsonBuilder().registerTypeAdapter(Company.class, new ClientCompanySerializer()).create();
                json = new String(gson.toJson(client, new TypeToken<Client<Company>>() {}.getType()));
            }
            return json;
        }

    }

    public static Client jsonToClient(String json) {
        Client client = null;

        try {

            Gson gson = new GsonBuilder().registerTypeAdapter(Person.class, new ClientPersonDeserializer()).create();
            client = gson.fromJson(json, new TypeToken<Client<Person>>() {}.getType());
            System.out.println("ClientConverter" + client.getClient().getClass());


        } catch (Exception e) {
            e.printStackTrace();

            Gson gson = new GsonBuilder().registerTypeAdapter(Company.class, new ClientCompanyDeserializer()).create();
            client = gson.fromJson(json, new TypeToken<Client<Company>>() {}.getType());
            System.out.println("ClientConverter" + client.getClient().getClass());
        }
        return client;
    }
}
