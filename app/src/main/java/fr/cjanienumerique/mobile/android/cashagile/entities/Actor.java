package fr.cjanienumerique.mobile.android.cashagile.entities;

import javax.mail.internet.InternetAddress;

import fr.cjanienumerique.mobile.android.cashagile.interfaces.ClientInterface;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.Editable;

public abstract class Actor {

    protected InternetAddress email;

    public InternetAddress getEmail() {
        return email;
    }

    public void setEmail(InternetAddress email) {
        this.email = email;
    }

}
