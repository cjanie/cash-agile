package fr.cjanienumerique.mobile.android.cashagile.ui.client;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import fr.cjanienumerique.mobile.android.cashagile.MainActivity;
import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.converters.ClientConverter;
import fr.cjanienumerique.mobile.android.cashagile.entities.Client;
import fr.cjanienumerique.mobile.android.cashagile.entities.Company;
import fr.cjanienumerique.mobile.android.cashagile.entities.Person;
import fr.cjanienumerique.mobile.android.cashagile.enums.ActionType;
import fr.cjanienumerique.mobile.android.cashagile.enums.ClientType;
import fr.cjanienumerique.mobile.android.cashagile.enums.Gender;
import fr.cjanienumerique.mobile.android.cashagile.enums.SuccessType;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.ClientInterface;
import fr.cjanienumerique.mobile.android.cashagile.ui.ClientFormFragment;
import fr.cjanienumerique.mobile.android.cashagile.ui.ClientPersonFormFragment;
import fr.cjanienumerique.mobile.android.cashagile.ui.ClientTypeFormFragment;
import fr.cjanienumerique.mobile.android.cashagile.ui.SuccessFragment;

public class ClientFragment extends Fragment {


    private ClientViewModel clientViewModel;

    private void showForm() {
        ClientFormFragment clientFormFragment = new ClientFormFragment();
        this.getFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_of_client_fragment, clientFormFragment)
                .addToBackStack(null)
                .commit();
    }

    private void observeTheClientToShowSuccess() {
        this.clientViewModel.getClient().observe(this, new Observer<Client>() {
            @Override
            public void onChanged(Client client) {
                if(client != null) {
                    SuccessFragment successFragment = new SuccessFragment();
                    successFragment.setSuccessType(SuccessType.CLIENT);
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.frame_of_client_fragment, successFragment)
                            .commit();
                }
            }
        });
    };

    // Override Fragment

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.clientViewModel = ViewModelProviders.of(this).get(ClientViewModel.class);

        View root = inflater.inflate(R.layout.fragment_client, container, false);

        this.showForm();
        this.observeTheClientToShowSuccess();

        return root;
    }

}