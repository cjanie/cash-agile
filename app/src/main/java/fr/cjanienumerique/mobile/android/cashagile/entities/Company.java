package fr.cjanienumerique.mobile.android.cashagile.entities;

import javax.mail.internet.InternetAddress;

import fr.cjanienumerique.mobile.android.cashagile.interfaces.ClientInterface;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.Editable;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.SessionInterface;

public class Company extends Actor implements ClientInterface, SessionInterface, Editable {

    private String name;
    private String reference;

    public Company() {
        this.name = "";
        this.reference = "";
    }

    public Company(String name) {
        this.name = name;
    }

    public Company (String name, String reference) {
        this.name = name;
        this.reference = reference;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }


    private String formatNameToUpperCase() {
        String upperCase = null;
        if(this.name != null) {
            upperCase = this.name.toUpperCase();
        }
        return upperCase;
    }

    public String formatCompanyToString() {
        return this.formatNameToUpperCase();


    }



    // implements ClientInterface

    @Override
    public String formatClientToString() {
        return this.formatCompanyToString();
    }

    @Override
    public InternetAddress getClientEmail() { // TODO tests
        return this.email;
    }



    // implements SessionInterface // TODO tests

    @Override
    public InternetAddress getSessionEmail() {
        return this.email;
    }


    @Override
    public void setSessionEmail(InternetAddress email) {
        this.email = email;
    }


    @Override
    public String formatSessionToString() {
        return this.formatCompanyToString();
    }



}
