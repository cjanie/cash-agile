package fr.cjanienumerique.mobile.android.cashagile.serializers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import fr.cjanienumerique.mobile.android.cashagile.converters.CompanyConverter;
import fr.cjanienumerique.mobile.android.cashagile.entities.Company;

public class ClientCompanySerializer implements JsonSerializer<Company> {
    @Override
    public JsonElement serialize(Company src, Type typeOfSrc, JsonSerializationContext context) {
        if(src == null) {
            return null;
        } else {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("client_company", CompanyConverter.companyToJson(src));
            return jsonObject;
        }

    }
}
