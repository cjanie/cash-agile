package fr.cjanienumerique.mobile.android.cashagile.interfaces;

import javax.mail.internet.InternetAddress;

public interface SessionInterface {

    String formatSessionToString();

    InternetAddress getSessionEmail();

    void setSessionEmail(InternetAddress email);


}
