package fr.cjanienumerique.mobile.android.cashagile.interfaces;

import javax.mail.internet.InternetAddress;

public interface ClientInterface {

    String formatClientToString();

    InternetAddress getClientEmail();

}
