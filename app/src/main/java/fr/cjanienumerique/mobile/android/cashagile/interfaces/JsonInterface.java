package fr.cjanienumerique.mobile.android.cashagile.interfaces;

public interface JsonInterface {

    String formatToJson();
}
