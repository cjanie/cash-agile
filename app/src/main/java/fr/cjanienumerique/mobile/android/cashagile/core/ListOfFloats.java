package fr.cjanienumerique.mobile.android.cashagile.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListOfFloats {

    private List<Float> floats;

    public ListOfFloats(List<Float> floats) {
        this.floats = floats;
    }

    public List<String> getSplitsBeforePoint() {
        List<String> splitsBeforePoint = new ArrayList<>();
        for(Float f: this.floats) {
            String floatToString = f.toString();
            int pointIndex = floatToString.indexOf(".");
            if(pointIndex > 0) {
                String beforePoint = floatToString.substring(0, pointIndex);
                splitsBeforePoint.add(beforePoint);
            } else {
                splitsBeforePoint.add(floatToString);
            }
        }
        return splitsBeforePoint;
    }

    public List<String> getSplitsAfterPoint() {
        List<String> splitsAfterPoint = new ArrayList<>();
        for(Float f: this.floats) {
            String floatToString = f.toString();
            int pointIndex = floatToString.indexOf(".");
            if(pointIndex > 0) {
                String afterPoint = floatToString.substring(pointIndex + 1);
                splitsAfterPoint.add(afterPoint);
            }
        }
        return splitsAfterPoint;
    }

    public int getSplitsBeforePointColumnWidth() {
        List<String> splitsBeforePoint = this.getSplitsBeforePoint();
        List<Integer> widthsBeforePoint = new ArrayList<>();
        for(String split: splitsBeforePoint) {
            widthsBeforePoint.add(split.length());
        }
        int maxWidthBeforePoint = 0;
        if(!widthsBeforePoint.isEmpty()) {
            Collections.sort(widthsBeforePoint);
            maxWidthBeforePoint = widthsBeforePoint.get(widthsBeforePoint.size() - 1);
        }
        return maxWidthBeforePoint;
    }

    public int getSplitsAfterPointColumnWidth() {
        List<String> splitsAfterPoint = this.getSplitsAfterPoint();
        List<Integer> widthsAfterPoint = new ArrayList<>();
        for(String split: splitsAfterPoint) {
            widthsAfterPoint.add(split.length());
        }
        int maxWidthAfterPoint = 0;
        if(!widthsAfterPoint.isEmpty()) {
            Collections.sort(widthsAfterPoint);
            maxWidthAfterPoint = widthsAfterPoint.get(widthsAfterPoint.size() - 1);
        }
        return maxWidthAfterPoint;
    }

    public List<Integer> getIndexOfFloatPoints() {
        List<Integer> indexOfPoints = new ArrayList<>();
        for(Float f: this.floats) {
            String floatToString = f.toString();
            int indexOfPoint = floatToString.indexOf(".");
            indexOfPoints.add(indexOfPoint);
        }
        return indexOfPoints;
    }

    public List<Integer> formatSplitsBeforePointToIntegers() {
        List<String> splitsBeforePoint = this.getSplitsBeforePoint();
        List<Integer> splitsToIntegers = new ArrayList<>();
        for(String splitBeforePoint: splitsBeforePoint) {
            int splitToInteger = Integer.parseInt(splitBeforePoint);
            splitsToIntegers.add(splitToInteger);
        }
        return splitsToIntegers;
    }

    public List<String> formatSplitsBeforePointToContentOfGrid() {
        return new ListOfIntegers(this.formatSplitsBeforePointToIntegers()).formatIntegersToContentOfGrid();
    }

    public List<String> formatSplitsAfterPointToContentOfGrid() {
        List<String> splitsAfterPoint = this.getSplitsAfterPoint();
        List<String> formatedSplits = new ArrayList<>();
        StringBuilder stringBuilder = null;
        for(String split: this.getSplitsAfterPoint()) {
            stringBuilder = new StringBuilder(split);
            while(stringBuilder.length() < this.getSplitsAfterPointColumnWidth()) {
                stringBuilder.append("0");
            }
            formatedSplits.add(stringBuilder.toString());
        }
        return formatedSplits;
    }

    public List<String> formatFloatsToContentOfGrid() {
        List<String> formatedFloats = new ArrayList<>();
        List<String> formatedSplitsBeforePoint = this.formatSplitsBeforePointToContentOfGrid();
        List<String> formatedSplitsAfterPoint = this.formatSplitsAfterPointToContentOfGrid();
        for(int i=0; i<formatedSplitsBeforePoint.size(); i++) {
            String formatedFloat = formatedSplitsBeforePoint.get(i) + "." + formatedSplitsAfterPoint.get(i);
            formatedFloats.add(formatedFloat);
        }
        return formatedFloats;
    }

}
