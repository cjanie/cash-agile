package fr.cjanienumerique.mobile.android.cashagile.managers;

import android.app.Application;
import android.os.Build;
import android.os.Environment;

import androidx.annotation.RequiresApi;

import java.io.File;

import fr.cjanienumerique.mobile.android.cashagile.enums.FileTarget;


public class PdfStorage {

    private File parent;
    private String parentPath;
    private String childName;

    protected FileTarget fileTarget;


    private static PdfStorage INSTANCE;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private PdfStorage(Application application) {
        this.parent = application.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        this.parentPath = this.parent.getPath();
        this.childName = "cash_agile_pdf";

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static PdfStorage getInstance(Application application) {
        if(PdfStorage.INSTANCE == null) {
            PdfStorage.INSTANCE = new PdfStorage(application);
        }
        return PdfStorage.INSTANCE;
    }




    public File getTargetFile() {
        File container = new File(this.parent, this.childName);
        if(!container.exists()) {
            container.mkdir();
        }
        File targetFile = new File(container, this.getTargetFileName());
        return targetFile;
    }

    public String getTargetFilePath() {
        return this.parentPath + "/" + this.childName + "/" + this.getTargetFileName();
    }

    public String getTargetFileName() {
        return "document.pdf";
    }
}
