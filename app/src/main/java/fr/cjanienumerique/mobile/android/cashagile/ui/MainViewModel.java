package fr.cjanienumerique.mobile.android.cashagile.ui;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.io.IOException;

import javax.mail.internet.AddressException;

import fr.cjanienumerique.mobile.android.cashagile.entities.Edition;
import fr.cjanienumerique.mobile.android.cashagile.entities.Quote;
import fr.cjanienumerique.mobile.android.cashagile.managers.FileStorageManagerSession;
import fr.cjanienumerique.mobile.android.cashagile.repositories.QuoteRepository;
import fr.cjanienumerique.mobile.android.cashagile.services.EditionService;


public class MainViewModel extends AndroidViewModel {

    // Properties **************

    private EditionService editionService;

    private QuoteRepository quoteRepository;

    private FileStorageManagerSession fileStorageManager;

    // Constructor ************

    public MainViewModel(@NonNull Application application) {
        super(application);
        this.editionService = EditionService.getInstance();
        this.quoteRepository = QuoteRepository.getInstance();
        this.fileStorageManager = FileStorageManagerSession.getInstance(application);

    }

    // Uses edition service ***************

    public void resetEditionService() {
        this.editionService.resetEditionService();
    }

    public MutableLiveData<Edition> getEdition() {
        return this.editionService.getEdition();
    }

    // Uses QuoteRepository with FileStorageManagerSession *****************

    public void saveQuote(Quote quote) throws IOException, AddressException {
        this.quoteRepository.saveQuote(quote, this.fileStorageManager);
    }



}
