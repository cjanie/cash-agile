package fr.cjanienumerique.mobile.android.cashagile.converters;

import com.google.gson.Gson;

import fr.cjanienumerique.mobile.android.cashagile.entities.Person;

public class PersonConverter {

    public static String personToJson(Person person) {
        if(person == null) {
            return null;
        } else {
            return new Gson().toJson(person);
        }
    }

    public static Person jsonToPerson(String json) {
        if(json == null) {
            return null;
        } else {
            return new Gson().fromJson(json, Person.class);
        }
    }
}
