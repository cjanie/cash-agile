package fr.cjanienumerique.mobile.android.cashagile.services;


import androidx.lifecycle.MutableLiveData;

import fr.cjanienumerique.mobile.android.cashagile.entities.Cart;
import fr.cjanienumerique.mobile.android.cashagile.entities.Client;
import fr.cjanienumerique.mobile.android.cashagile.entities.Company;
import fr.cjanienumerique.mobile.android.cashagile.entities.Edition;

public class EditionService {

    private static EditionService INSTANCE;

    private MutableLiveData<Edition> edition;




    private EditionService() {
        this.edition = new MutableLiveData<Edition>();
        this.edition.setValue(new Edition());
    }

    public static EditionService getInstance() {
        if(EditionService.INSTANCE == null) {
            EditionService.INSTANCE = new EditionService();
        }
        return EditionService.INSTANCE;
    }

    public void setCart(Cart cart) {
        this.edition.getValue().setCart(cart);
    }

    public void setClient(Client client) {
        this.edition.getValue().setClient(client);
    }

    public void setCompany(Company company) {
        this.edition.getValue().setCompany(company);
    }

    public MutableLiveData<Edition> getEdition() {
        return this.edition;
    }

    public void resetEditionService() {
        this.edition.setValue(new Edition());
    }

}
