package fr.cjanienumerique.mobile.android.cashagile.deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import fr.cjanienumerique.mobile.android.cashagile.converters.CartConverter;
import fr.cjanienumerique.mobile.android.cashagile.converters.ClientConverter;
import fr.cjanienumerique.mobile.android.cashagile.converters.CompanyConverter;
import fr.cjanienumerique.mobile.android.cashagile.entities.Cart;
import fr.cjanienumerique.mobile.android.cashagile.entities.Client;
import fr.cjanienumerique.mobile.android.cashagile.entities.Company;
import fr.cjanienumerique.mobile.android.cashagile.entities.Edition;

public class EditionDeserializer implements JsonDeserializer<Edition> {
    @Override
    public Edition deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if(json == null) {
            return null;
        } else {
            String jsonCompany = json.getAsJsonObject().get("company").getAsString();
            String jsonClient = json.getAsJsonObject().get("client").getAsString();
            String jsonCart = json.getAsJsonObject().get("cart").getAsString();
            Company company = CompanyConverter.jsonToCompany(jsonCompany);
            Client client = ClientConverter.jsonToClient(jsonClient);
            Cart cart = CartConverter.jsonToCart(jsonCart);
            Edition edition = new Edition();
            edition.setCompany(company);
            edition.setClient(client);
            edition.setCart(cart);
            return edition;
        }

    }
}
