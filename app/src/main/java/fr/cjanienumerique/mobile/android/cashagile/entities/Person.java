package fr.cjanienumerique.mobile.android.cashagile.entities;

import javax.mail.internet.InternetAddress;

import fr.cjanienumerique.mobile.android.cashagile.enums.Gender;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.ClientInterface;

public class Person extends Actor implements ClientInterface {

    private Gender gender;
    private String firstName;
    private String lastName;



    public Person(Gender gender, String firstName, String lastName) {
        this.gender = gender;
        this.firstName = firstName;
        this.lastName = lastName;
    }


    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    // functions

    public String formatFirstName() {
        StringBuilder formated = new StringBuilder();
        String lowerCase = this.firstName.toLowerCase();

        String[] split = lowerCase.split("-");
        for(String partOfName: split) {
            String firstLetter = String.valueOf(partOfName.charAt(0));
            StringBuilder formatedPartOfName = new StringBuilder(firstLetter.toUpperCase() + partOfName.substring(1));
            for(int i=0; i<split.length - 1; i++) {
               formatedPartOfName.append("-");
            }
            formated.append(formatedPartOfName);
        }
        String result = "";
        if(formated.indexOf("-") > 0) {
            result = formated.substring(0, formated.length() - 1).toString();
        } else {
            result = formated.toString();
        }
        return result;
    }

    public String formatLastNameToUpperCase() {
        return this.lastName.toUpperCase();
    }

    public String formatPersonCivilitiesToString() {
        StringBuilder stringBuilder = new StringBuilder();

        if(this.firstName != null && this.lastName != null) {
            if(this.gender != null) {
                if(this.gender == Gender.M) {
                    stringBuilder.append(Gender.M.toString() + " ");
                } else if(this.gender == Gender.MME) {
                    stringBuilder.append(Gender.MME.toString() + " ");
                }
            }
            stringBuilder.append(this.formatFirstName() + " " + this.formatLastNameToUpperCase());
        }
        return stringBuilder.toString();
    }


    @Override
    public String formatClientToString() {
        return this.formatPersonCivilitiesToString();
    }

    @Override
    public InternetAddress getClientEmail() {
        return this.email;
    }

}
