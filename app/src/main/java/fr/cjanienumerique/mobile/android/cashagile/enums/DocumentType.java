package fr.cjanienumerique.mobile.android.cashagile.enums;

public enum DocumentType {
    QUOTE, BILL
}
