package fr.cjanienumerique.mobile.android.cashagile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.entities.Command;

public class CommandAdapter extends ArrayAdapter {

    private List<Command> commands;

    public CommandAdapter(@NonNull Context context, int resource, @NonNull List<Command> commands) {
        super(context, resource);
        this.commands = commands;
    }

    @Nullable
    @Override
    public Command getItem(int position) {
        return this.commands.get(position);
    }


    @Override
    public int getCount() {
        return this.commands.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Command command = this.getItem(position);
        LayoutInflater inflater = LayoutInflater.from(getContext());

        View view = inflater.inflate(R.layout.model_command_item, null);
        ((TextView) view.findViewById(R.id.cart_grid_quantity)).setText(command.getQuantity().toString());
        ((TextView) view.findViewById(R.id.cart_grid_item_name)).setText(command.getItem().getName());
        ((TextView) view.findViewById(R.id.cart_grid_unity_price)).setText(command.getItem().getPrice().toString());
        ((TextView) view.findViewById(R.id.cart_grid_total_command)).setText(command.getTotalCommand().toString());

        return view;
    }
}
