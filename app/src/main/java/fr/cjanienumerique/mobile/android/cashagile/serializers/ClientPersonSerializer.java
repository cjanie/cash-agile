package fr.cjanienumerique.mobile.android.cashagile.serializers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import fr.cjanienumerique.mobile.android.cashagile.converters.PersonConverter;
import fr.cjanienumerique.mobile.android.cashagile.entities.Person;
import fr.cjanienumerique.mobile.android.cashagile.enums.Gender;

public class ClientPersonSerializer implements JsonSerializer<Person> {

    @Override
    public JsonElement serialize(Person src, Type typeOfSrc, JsonSerializationContext context) {
        if(src == null) {
            return null;
        } else {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("client_person", PersonConverter.personToJson(src));
            return jsonObject;
        }
    }
}
