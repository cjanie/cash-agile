package fr.cjanienumerique.mobile.android.cashagile.serializers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import fr.cjanienumerique.mobile.android.cashagile.converters.CartConverter;
import fr.cjanienumerique.mobile.android.cashagile.converters.ClientConverter;
import fr.cjanienumerique.mobile.android.cashagile.converters.CompanyConverter;
import fr.cjanienumerique.mobile.android.cashagile.entities.Edition;

public class EditionSerializer implements JsonSerializer<Edition> {
    @Override
    public JsonElement serialize(Edition src, Type typeOfSrc, JsonSerializationContext context) {
        if(src == null) {
            return null;
        } else {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("company", CompanyConverter.companyToJson(src.getCompany()));
            jsonObject.addProperty("client", ClientConverter.clientToJson(src.getClient()));
            jsonObject.addProperty("cart", CartConverter.cartToJson(src.getCart()));
            return jsonObject;
        }
    }
}
