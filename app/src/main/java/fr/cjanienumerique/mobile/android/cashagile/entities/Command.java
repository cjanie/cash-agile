package fr.cjanienumerique.mobile.android.cashagile.entities;

import fr.cjanienumerique.mobile.android.cashagile.exceptions.InvalidQuantityException;

public class Command {

    private Integer quantity;
    private Item item;


    public Command(Integer quantity, Item item) throws InvalidQuantityException {
        if(quantity < 0) {
            throw new InvalidQuantityException();
        } else {
            this.quantity = quantity;
            this.item = item;
        }

    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) throws InvalidQuantityException {
        if(quantity < 0) {
            throw new InvalidQuantityException();
        } else {
            this.quantity = quantity;
        }
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Float getTotalCommand() {

        return this.quantity * this.item.getPrice();
    }
}
