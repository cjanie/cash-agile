package fr.cjanienumerique.mobile.android.cashagile.converters;

import com.google.gson.Gson;

import fr.cjanienumerique.mobile.android.cashagile.entities.Cart;

public class CartConverter {

    public static String cartToJson(Cart cart) {
        if(cart == null) {
            return null;
        } else {
            Gson gson = new Gson();
            String json = gson.toJson(cart);
            return json;
        }
    }

    public static Cart jsonToCart(String json) {
        if(json == null) {
            return null;
        } else {
            Gson gson = new Gson();
            Cart cart = gson.fromJson(json, Cart.class);
            return cart;
        }
    }
}
