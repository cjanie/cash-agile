package fr.cjanienumerique.mobile.android.cashagile.interfaces;

import java.util.List;

public interface Printable {

    String printAsString();

}
