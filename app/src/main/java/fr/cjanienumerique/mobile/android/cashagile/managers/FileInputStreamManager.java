package fr.cjanienumerique.mobile.android.cashagile.managers;

import java.io.FileInputStream;
import java.io.IOException;

public class FileInputStreamManager {

    private static FileInputStreamManager INSTANCE;

    private FileInputStreamManager() {}

    public static FileInputStreamManager getInstance() {
        if(FileInputStreamManager.INSTANCE == null) {
            FileInputStreamManager.INSTANCE = new FileInputStreamManager();
        }
        return FileInputStreamManager.INSTANCE;
    }

    public String read(FileInputStream fileInputStream) throws IOException {
        String string = "";
        byte[] bytes = new byte[128];

        // loop for complete read up to end
        try {
            while(true) {
                int hasBeenReading = fileInputStream.read(bytes);
                if(hasBeenReading < 0) {
                    // - 1 nothing more to read
                    break;
                }
                string += new String(bytes, 0, hasBeenReading); // the file content as a string
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if(fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
        }
        System.out.println("FileInputStreamManager read done: " + string);
        return string;
    }
}
