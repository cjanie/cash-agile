package fr.cjanienumerique.mobile.android.cashagile.services;


import java.io.IOException;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import fr.cjanienumerique.mobile.android.cashagile.managers.FileStorageManagerSession;
import fr.cjanienumerique.mobile.android.cashagile.repositories.SessionRepository;

public class ConfigurationService {

    private InternetAddress email; // gmail address

    private String password; // gmail password

    private SessionRepository sessionRepository;

    private static ConfigurationService INSTANCE;

    private ConfigurationService() {
        this.sessionRepository = SessionRepository.getInstance();
    };

    public static ConfigurationService getInstance() {
        if(ConfigurationService.INSTANCE == null) {
            ConfigurationService.INSTANCE = new ConfigurationService();
        }
        return ConfigurationService.INSTANCE;
    }


    public InternetAddress getEmail(FileStorageManagerSession fileStorageManager) throws IOException, AddressException {
        if(this.sessionRepository.getSession(fileStorageManager).getValue() != null) {
            if(this.sessionRepository.getSession(fileStorageManager).getValue().getSession().getSessionEmail() != null) {
                this.email = new InternetAddress(this.sessionRepository.getSession(fileStorageManager).getValue().getSession().getSessionEmail().getAddress());
            }
        }
        return this.email;
    }


    public String getPassword(FileStorageManagerSession fileStorageManager) throws IOException, AddressException {
        if(this.sessionRepository.getSession(fileStorageManager).getValue() != null) {
            if(this.sessionRepository.getSession(fileStorageManager).getValue().getEmailPassword() != null) {
                this.password = new String(this.sessionRepository.getSession(fileStorageManager).getValue().getEmailPassword());
            }
        }
        return this.password;
    }

}
