package fr.cjanienumerique.mobile.android.cashagile.ui.company;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.entities.Company;
import fr.cjanienumerique.mobile.android.cashagile.entities.Quote;
import fr.cjanienumerique.mobile.android.cashagile.entities.Session;
import fr.cjanienumerique.mobile.android.cashagile.enums.SuccessType;
import fr.cjanienumerique.mobile.android.cashagile.ui.CompanyFormFragment;
import fr.cjanienumerique.mobile.android.cashagile.ui.SessionFormFragment;
import fr.cjanienumerique.mobile.android.cashagile.ui.SuccessFragment;

public class CompanyFragment extends Fragment {

    private CompanyViewModel companyViewModel;

    private void showForm() {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_of_company_fragment, new SessionFormFragment())
                .commit();
    }

    private void observeTheCompanySessionToShowSuccess() {
        try {
            this.companyViewModel.getSession().observe(this, new Observer<Session>() {
                @Override
                public void onChanged(Session session) {
                    if(session != null) {
                        Company company = session.getSession();
                        if(company != null) {
                            companyViewModel.setCompanyToEdition(company); // !!!!!!!!!!
                            SuccessFragment successFragment = new SuccessFragment();
                            successFragment.setSuccessType(SuccessType.COMPANY);
                            getFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.frame_of_company_fragment, successFragment)
                                    .commit();
                        }
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (AddressException e) {
            e.printStackTrace();
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        this.companyViewModel = ViewModelProviders.of(this).get(CompanyViewModel.class);

        View root = inflater.inflate(R.layout.fragment_company, container, false);


        this.showForm();

        this.observeTheCompanySessionToShowSuccess();


        return root;
    }

}