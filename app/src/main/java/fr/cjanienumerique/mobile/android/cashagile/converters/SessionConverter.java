package fr.cjanienumerique.mobile.android.cashagile.converters;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import fr.cjanienumerique.mobile.android.cashagile.deserializers.QuoteDeserializer;

import fr.cjanienumerique.mobile.android.cashagile.entities.Quote;
import fr.cjanienumerique.mobile.android.cashagile.entities.Session;
import fr.cjanienumerique.mobile.android.cashagile.serializers.QuoteSerialiser;


public class SessionConverter {

    public static String SessionToJson(Session session) {
        if(session == null) {
            return null;
        } else {
            Gson gson = new GsonBuilder().registerTypeAdapter(Quote.class, new QuoteSerialiser()).create();
            String json = gson.toJson(session, new TypeToken<Session>() {}.getType());
            return json;
            //return new Gson().toJson(session);
        }
    }

    public static Session jsonToSession(String json) {
        if(json == null) {
            return null;
        } else {
            Gson gson = new GsonBuilder().registerTypeAdapter(Quote.class, new QuoteDeserializer()).create();
            Session session = gson.fromJson(json, new TypeToken<Session>() {}.getType());
            return session;
            //return new Gson().fromJson(json, Session.class);
        }
    }
}
