package fr.cjanienumerique.mobile.android.cashagile.services;

import android.os.AsyncTask;

import java.security.Security;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class GmailAsyncSender extends AsyncTask {

    private final String MAIL_HOST = "smtp.gmail.com";

    private String user;

    private String password;

    private InternetAddress recipient;

    private String subject;

    private String body;

    private Session javaxMailSession;

    static {
        Security.addProvider(new JSSEProvider());
    }

    public GmailAsyncSender(String user, String password) {
        this.user = user;
        this.password = password;
        Properties properties = new Properties();
        properties.setProperty("mail.transport.protocol", "smtp");
        properties.setProperty("mail.host", this.MAIL_HOST);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.socketFactory.fallback", "false");
        properties.setProperty("mail.smtp.quitwait", "false");

        this.javaxMailSession = Session.getDefaultInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, password);
            }
        });

        this.recipient = recipient;
        this.subject = subject;
        this.body = body;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setRecipient(InternetAddress recipient) {
        this.recipient = recipient;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        try {
            this.sendEmail();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void sendEmail() throws MessagingException {
        if(this.recipient != null) {
            MimeMessage mimeMessage = new MimeMessage(this.javaxMailSession);
            DataHandler dataHandler = new DataHandler(new ByteArrayDataSource(this.body.getBytes(), "text/plain"));

            mimeMessage.setSender(new InternetAddress(this.user));
            mimeMessage.setSubject(this.subject);
            mimeMessage.setDataHandler(dataHandler);
            mimeMessage.setRecipient(Message.RecipientType.TO, this.recipient);
            Transport.send(mimeMessage);
        }
    }
}
