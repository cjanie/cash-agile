package fr.cjanienumerique.mobile.android.cashagile.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.entities.Client;
import fr.cjanienumerique.mobile.android.cashagile.entities.Company;
import fr.cjanienumerique.mobile.android.cashagile.enums.SuccessType;
import fr.cjanienumerique.mobile.android.cashagile.ui.client.ClientViewModel;

public class ClientCompanyFormFragment extends Fragment implements View.OnClickListener {

    private ClientViewModel clientViewModel;

    private EditText companyNameView;
    private EditText emailView;
    private TextView errorView;
    private FloatingActionButton fabValid;


    private String companyName;
    private String email;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.clientViewModel = ViewModelProviders.of(this).get(ClientViewModel.class);

        View root = inflater.inflate(R.layout.fragment_client_company_form, container, false);
        this.companyNameView = root.findViewById(R.id.client_company_name);
        this.emailView = root.findViewById(R.id.email);
        this.errorView = root.findViewById(R.id.error);
        this.fabValid = root.findViewById(R.id.fab_valid);

        this.fabValid.setOnClickListener(this);


        this.clientViewModel.getClient().observe(this, new Observer<Client>() {
            @Override
            public void onChanged(Client client) {
                if(client != null) {
                    if(client.getClient() != null) {
                        if(client.getClient() instanceof Company) {
                            companyName = ((Company) client.getClient()).getName();
                            email = ((Company) client.getClient()).getEmail().getAddress();
                        }
                    }
                }
            }
        });
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.companyNameView.setText(this.companyName);
        this.emailView.setText(this.email);
    }

    @Override
    public void onClick(View v) {

        Client client = null;

        Company company = null;
        if (TextUtils.isEmpty(this.companyNameView.getText())) {
            this.errorView.setText(getContext().getString(R.string.error_name_company));
        } else if (TextUtils.isEmpty(this.emailView.getText())) {
            this.errorView.setText(getContext().getString(R.string.error_email));
        } else {
            // clear the error message
            this.errorView.setText("");

            // instanciate company
            company = new Company(companyNameView.getText().toString());
            // set email
            try {
                InternetAddress email = new InternetAddress(emailView.getText().toString());
                company.setEmail(email);

                // set Company as Client
                client = new Client<Company>();
                client.setClient(company);

                // valid client
                clientViewModel.validClient(client);
                System.out.println(client.serialize());

                // Show success message
                SuccessFragment successFragment = new SuccessFragment();
                successFragment.setSuccessType(SuccessType.CLIENT);
                getParentFragment().getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frame_of_client_fragment, successFragment)
                        .commit(); // No addToBackStack > will return to Cart !!!!!!!!! VERY OK
            } catch (AddressException e) {
                errorView.setText(getContext().getString(R.string.error_email_invalid));

            }

        }
    }
}
