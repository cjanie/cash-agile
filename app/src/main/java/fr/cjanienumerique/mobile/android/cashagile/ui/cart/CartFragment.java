package fr.cjanienumerique.mobile.android.cashagile.ui.cart;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.FloatingActionButton;


import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.adapters.CommandAdapter;
import fr.cjanienumerique.mobile.android.cashagile.entities.Cart;
import fr.cjanienumerique.mobile.android.cashagile.entities.Command;
import fr.cjanienumerique.mobile.android.cashagile.ui.CashDeskBoardFragment;
import fr.cjanienumerique.mobile.android.cashagile.ui.CashDeskFragment;
import fr.cjanienumerique.mobile.android.cashagile.ui.CommandFormFragment;
import fr.cjanienumerique.mobile.android.cashagile.ui.ConfirmFragment;


public class CartFragment extends Fragment {

    private CartViewModel cartViewModel;

    private Integer selectedCommandIndex;


    private void showCommandForm() {
        if(this.selectedCommandIndex == null) {
            this.getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_of_cart_fragment, new CommandFormFragment())
                    .commit();
        } else {
            CommandFormFragment commandFormFragment = new CommandFormFragment();
            commandFormFragment.setSelectedCommandIndex(selectedCommandIndex);
        }

    }

    private void observeCartToShowCashDeskBoard() {
        if(selectedCommandIndex == null) {
            this.cartViewModel.getCart().observe(this, new Observer<Cart>() {
                @Override
                public void onChanged(Cart cart) {

                    if (cart != null) {
                        if (selectedCommandIndex == null) {
                            if (!cart.getCommands().isEmpty()) {
                                CashDeskBoardFragment cashDeskBoardFragment = new CashDeskBoardFragment();
                                getFragmentManager()
                                        .beginTransaction()
                                        .replace(R.id.frame_of_cart_fragment, cashDeskBoardFragment)
                                        .commit();
                            }
                        } else {
                            try {
                                cartViewModel.getFromCart(selectedCommandIndex).observe(CartFragment.this, new Observer<Command>() {
                                    @Override
                                    public void onChanged(Command command) {
                                        if (command != null) {
                                            CommandFormFragment commandFormFragment = new CommandFormFragment();
                                            commandFormFragment.setSelectedCommandIndex(selectedCommandIndex);
                                            getFragmentManager()
                                                    .beginTransaction()
                                                    .replace(R.id.frame_of_cart_fragment, commandFormFragment)
                                                    .commit();
                                        }
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });

        }
    }










    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_cart, container, false);


        // instanciate properties
        this.cartViewModel = ViewModelProviders.of(this).get(CartViewModel.class);
        this.selectedCommandIndex = null;



        this.showCommandForm();
        this.observeCartToShowCashDeskBoard();



        return root;
    }










    // private cart managing functions
    private void addToCart(Command command) throws Exception {
        this.cartViewModel.addToCart(command);
    }

    private void updateCart(Command command) throws Exception {
        this.cartViewModel.updateCart(this.selectedCommandIndex, command);
        this.selectedCommandIndex = null;
    }

    private void deleteCommand() throws Exception {
            this.cartViewModel.removeFromCart(this.selectedCommandIndex);
            this.selectedCommandIndex = null;
    }


    private void showConfirmFragment() throws Exception {
        ConfirmFragment confirmFragment = new ConfirmFragment();
        confirmFragment.setConfirmMessage(buildConfirmDeleteMessage(cartViewModel.getFromCart(this.selectedCommandIndex).getValue()));
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_of_cart_fragment, confirmFragment)
                .commit();
    }




    private String buildConfirmDeleteMessage(Command command) {
        StringBuilder stringBuilder = new StringBuilder(getContext().getString(R.string.confirm_delete));
        stringBuilder.append(command.getQuantity() + " " + command.getItem().getName());
        return stringBuilder.toString();
    }



}