package fr.cjanienumerique.mobile.android.cashagile.ui.cart;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.IOException;

import javax.mail.internet.AddressException;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.entities.Cart;
import fr.cjanienumerique.mobile.android.cashagile.entities.Command;
import fr.cjanienumerique.mobile.android.cashagile.entities.Edition;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.Editable;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.SuccessMessageBuilder;
import fr.cjanienumerique.mobile.android.cashagile.services.EditionService;

public class CartViewModel extends AndroidViewModel implements SuccessMessageBuilder {

    private EditionService editionService;

    public CartViewModel(@NonNull Application application) {
        super(application);
        this.editionService = EditionService.getInstance();
    }

    private void validCart(Cart cart) {
        this.editionService.setCart(cart);
    }

    public MutableLiveData<Cart> getCart() {
        MutableLiveData<Cart> cart = new MutableLiveData<>();
        if(this.editionService.getEdition().getValue().getCart() != null) {
            cart.setValue(this.editionService.getEdition().getValue().getCart());
        }
        return cart;
    }

    public void addToCart(Command command) throws Exception {
        if(this.editionService.getEdition().getValue().getCart() == null) {
            Cart cart = new Cart();
            this.validCart(cart);
            this.editionService.getEdition().getValue().getCart().addCommand(command);
        } else {
            this.editionService.getEdition().getValue().getCart().addCommand(command);
        }
    }

    public void updateCart(int index, Command command) throws Exception {
        if(this.editionService.getEdition().getValue().getCart() != null) {
            this.editionService.getEdition().getValue().getCart().updateCommand(index, command);
        }
    }

    public void removeFromCart(int index) throws Exception {
        if(this.editionService.getEdition().getValue().getCart() != null) {
            this.editionService.getEdition().getValue().getCart().deleteCommand(index);
        }
    }

    public MutableLiveData<Command> getFromCart(int index) throws Exception {
        MutableLiveData<Command> command = new MutableLiveData<>();
        if(this.editionService.getEdition().getValue().getCart() != null) {
            command.setValue(this.editionService.getEdition().getValue().getCart().getCommand(index));
        }
        return command;
    }

    @Override
    public String buildSuccessMessage(Editable editable) {
        int numberOfItems = ((Cart) editable).getNumberOfItems();
        StringBuilder stringBuilder
                = new StringBuilder(this.getApplication().getString(R.string.cart) + " : "
                + numberOfItems + " " + this.getApplication().getString(R.string.item).toLowerCase());
        if(numberOfItems > 1) {
            stringBuilder.append("s");
        }
        return stringBuilder.toString();
    }

}