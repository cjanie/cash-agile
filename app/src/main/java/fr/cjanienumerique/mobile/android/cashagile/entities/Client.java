package fr.cjanienumerique.mobile.android.cashagile.entities;

import fr.cjanienumerique.mobile.android.cashagile.converters.ClientConverter;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.Convertible;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.Editable;

public class Client<ClientInterface> implements Convertible, Editable {

    private ClientInterface client;

    public void setClient(ClientInterface client) {
        this.client = client;
    }

    public ClientInterface getClient() {
        return this.client;
    }

    @Override
    public String serialize() {
        return ClientConverter.clientToJson(this);
    }
}
