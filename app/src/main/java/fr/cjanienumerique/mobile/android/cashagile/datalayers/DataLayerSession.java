package fr.cjanienumerique.mobile.android.cashagile.datalayers;



import androidx.lifecycle.MutableLiveData;

import java.io.IOException;

import fr.cjanienumerique.mobile.android.cashagile.converters.SessionConverter;
import fr.cjanienumerique.mobile.android.cashagile.entities.Session;
import fr.cjanienumerique.mobile.android.cashagile.managers.BufferedReaderManager;
import fr.cjanienumerique.mobile.android.cashagile.managers.FileStorageManagerSession;
import fr.cjanienumerique.mobile.android.cashagile.managers.FileWriterManager;

public class DataLayerSession {

    private FileWriterManager fileWriterManager;
    private BufferedReaderManager bufferedReaderManager;

    private static DataLayerSession INSTANCE;



    private DataLayerSession() {
        this.fileWriterManager = FileWriterManager.getInstance();
        this.bufferedReaderManager = BufferedReaderManager.getInstance();
    }

    public static DataLayerSession getInstance() {
        if(DataLayerSession.INSTANCE == null) {
            DataLayerSession.INSTANCE = new DataLayerSession();
        }
        return DataLayerSession.INSTANCE;
    }



    public void saveSession(Session session, FileStorageManagerSession fileStorageManager) throws IOException {
        try {
            this.writeFile(session, fileStorageManager);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }


    public MutableLiveData<Session>  getSession(FileStorageManagerSession fileStorageManager) throws IOException {
        MutableLiveData<Session> mutableLiveData = new MutableLiveData<>();
        String json = this.readFile(fileStorageManager);
        Session session = SessionConverter.jsonToSession(json);
        mutableLiveData.setValue(session);
        return mutableLiveData;
    }



    private void writeFile(Session session, FileStorageManagerSession fileStorageManager) throws IOException {

        // write the session as a string
        String sessionAsJsonString = session.serialize();

        try {
            this.fileWriterManager.write(sessionAsJsonString, fileStorageManager);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }

    }

    private String readFile(FileStorageManagerSession fileStorageManager) throws IOException {
        return this.bufferedReaderManager.read(fileStorageManager);
    }

}
