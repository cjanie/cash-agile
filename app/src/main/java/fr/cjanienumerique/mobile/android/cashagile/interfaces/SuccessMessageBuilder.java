package fr.cjanienumerique.mobile.android.cashagile.interfaces;

public interface SuccessMessageBuilder {

    String buildSuccessMessage(Editable editable);

}
