package fr.cjanienumerique.mobile.android.cashagile.interfaces;

import java.util.List;

public interface PrintableGrid {

    List <String[]> printGrid();
}
