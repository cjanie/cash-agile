package fr.cjanienumerique.mobile.android.cashagile.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import fr.cjanienumerique.mobile.android.cashagile.converters.CompanyConverter;
import fr.cjanienumerique.mobile.android.cashagile.converters.SessionConverter;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.Convertible;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.Loadable;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.Saveable;

public class Session implements Convertible {

    private Company session;

    private String emailPassword; // gmail configuration service session

    private List<Quote> quotes;


    public Session() {
        this.emailPassword = "";
        this.quotes = new ArrayList<>();
    }

    public Company getSession() {
        return session;
    }

    public void setSession(Company session) {
        this.session = session;
    }

    public String getEmailPassword() {
        return emailPassword;
    }

    public void setEmailPassword(String emailPassword) {
        this.emailPassword = emailPassword;
    }

    public List<Quote> getQuotes() {
        return quotes;
    }

    public void setQuotes(List<Quote> quotes) {
        this.quotes = quotes;
    }

    @Override
    public String serialize() {
        return SessionConverter.SessionToJson(this);
    }


}
