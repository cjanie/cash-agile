package fr.cjanienumerique.mobile.android.cashagile.entities;

import fr.cjanienumerique.mobile.android.cashagile.exceptions.InvalidPriceException;

public class Item {

    private String name;
    private Float price;

    public Item(String name, Float price) throws InvalidPriceException {
        if(price < 0) {
            throw new InvalidPriceException();
        } else {
            this.name = name;
            this.price = price;
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) throws InvalidPriceException {
        if(price < 0) {
            throw new InvalidPriceException();
        } else {
            this.price = price;
        }
    }
}
