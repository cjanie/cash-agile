package fr.cjanienumerique.mobile.android.cashagile.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.entities.Cart;
import fr.cjanienumerique.mobile.android.cashagile.ui.cart.CartFragment;
import fr.cjanienumerique.mobile.android.cashagile.ui.cart.CartViewModel;

public class ConfirmFragment extends Fragment implements View.OnClickListener {

    private CartViewModel cartViewModel;

    private Integer selectedCommandIndex;

    private TextView confirmMessageView;

    private Button buttonCancel;
    private Button buttonConfirm;

    private String confirmMessage;

    public void setSelectedCommandIndex(Integer selectedCommandIndex) {
        this.selectedCommandIndex = selectedCommandIndex;
    }

    public void setConfirmMessage(String confirmMessage) {
        this.confirmMessage = confirmMessage;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.cartViewModel = ViewModelProviders.of(this).get(CartViewModel.class);

        View root = inflater.inflate(R.layout.fragment_confirm, container, false);

        this.confirmMessageView = root.findViewById(R.id.confirm_message);
        this.buttonCancel = root.findViewById(R.id.button_cancel);
        this.buttonConfirm = root.findViewById(R.id.button_confirm);

        this.buttonCancel.setOnClickListener(this);
        this.buttonConfirm.setOnClickListener(this);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.confirmMessageView.setText(this.confirmMessage);
    }

    @Override
    public void onClick(View v) {
        if(v == this.buttonCancel) {
            CashDeskBoardFragment cashDeskBoardFragment = new CashDeskBoardFragment();
            this.getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_of_cart_fragment, cashDeskBoardFragment)
                    .commit();

        } else if(v == this.buttonConfirm) {
            try {
                if(this.selectedCommandIndex != null) {
                    this.cartViewModel.removeFromCart(this.selectedCommandIndex);
                }

                this.selectedCommandIndex = null;

                cartViewModel.getCart().observe(this, new Observer<Cart>() {
                    @Override
                    public void onChanged(Cart cart) {
                        if(cart != null) {
                            if(cart.getCommands().isEmpty()) {
                                CommandFormFragment commandFormFragment = new CommandFormFragment();
                                getFragmentManager()
                                        .beginTransaction()
                                        .replace(R.id.frame_of_cart_fragment, commandFormFragment)
                                        .commit();

                            } else if(cart.getCommands().size() > 0) {
                                CashDeskBoardFragment cashDeskBoardFragment = new CashDeskBoardFragment();
                                getFragmentManager()
                                        .beginTransaction()
                                        .replace(R.id.frame_of_cart_fragment, cashDeskBoardFragment)
                                        .commit();
                            }
                        }



                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }



        }
    }
}
