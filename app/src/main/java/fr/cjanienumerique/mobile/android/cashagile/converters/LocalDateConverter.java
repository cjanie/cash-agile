package fr.cjanienumerique.mobile.android.cashagile.converters;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.room.TypeConverter;

import java.time.LocalDate;

public class LocalDateConverter {

    private LocalDateConverter() {
        throw new AssertionError();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @TypeConverter
    public static LocalDate fromLong(Long longValue) {
        return longValue == null ? null : LocalDate.ofEpochDay(longValue);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @TypeConverter
    public static Long fromLocalDate(LocalDate localDate) {
        return localDate == null ? null : localDate.toEpochDay();
    }

}
