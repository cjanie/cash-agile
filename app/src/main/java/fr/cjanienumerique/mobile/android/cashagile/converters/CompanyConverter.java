package fr.cjanienumerique.mobile.android.cashagile.converters;

import com.google.gson.Gson;

import fr.cjanienumerique.mobile.android.cashagile.entities.Company;

public class CompanyConverter {

    public static String companyToJson(Company company) {
        if (company == null) {
            return null;
        } else {
            return new Gson().toJson(company);
        }
    }

    public static Company jsonToCompany(String json) {
        if(json == null) {
            return null;
        } else {
            return new Gson().fromJson(json, Company.class);
        }
    }
}
