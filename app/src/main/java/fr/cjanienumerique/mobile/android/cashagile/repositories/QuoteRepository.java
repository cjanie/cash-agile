package fr.cjanienumerique.mobile.android.cashagile.repositories;

import androidx.lifecycle.MutableLiveData;

import java.io.IOException;
import java.util.List;


import javax.mail.internet.AddressException;

import fr.cjanienumerique.mobile.android.cashagile.datalayers.DataLayerQuote;
import fr.cjanienumerique.mobile.android.cashagile.entities.Quote;
import fr.cjanienumerique.mobile.android.cashagile.managers.FileStorageManagerSession;

public class QuoteRepository {

    private DataLayerQuote dataLayerQuote;

    private static QuoteRepository INSTANCE;

    private QuoteRepository() {
        this.dataLayerQuote = DataLayerQuote.getInstance();
    }

    public static QuoteRepository getInstance() {
        if(QuoteRepository.INSTANCE == null) {
            QuoteRepository.INSTANCE = new QuoteRepository();
        }
        return QuoteRepository.INSTANCE;
    }


    public void saveQuote(Quote quote, FileStorageManagerSession fileStorageManager) throws IOException, AddressException {
        this.dataLayerQuote.saveQuote(quote, fileStorageManager);
    }

    public MutableLiveData<List<Quote>> getQuotes(FileStorageManagerSession fileStorageManager) throws IOException, AddressException {
        return this.dataLayerQuote.getQuotes(fileStorageManager);
    }

    public MutableLiveData<Quote> getLastQuote(FileStorageManagerSession fileStorageManager) throws IOException, AddressException {
        return this.dataLayerQuote.getLastQuote(fileStorageManager);
    }

}
