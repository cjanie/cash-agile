package fr.cjanienumerique.mobile.android.cashagile.entities;

import fr.cjanienumerique.mobile.android.cashagile.interfaces.ClientInterface;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.SessionInterface;

public class Edition {

    private Company company;
    private Client client;
    private Cart cart;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }
}
