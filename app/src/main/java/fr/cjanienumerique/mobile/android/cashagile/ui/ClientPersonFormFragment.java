package fr.cjanienumerique.mobile.android.cashagile.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.entities.Client;
import fr.cjanienumerique.mobile.android.cashagile.entities.Person;
import fr.cjanienumerique.mobile.android.cashagile.enums.Gender;
import fr.cjanienumerique.mobile.android.cashagile.enums.SuccessType;
import fr.cjanienumerique.mobile.android.cashagile.ui.client.ClientViewModel;

public class ClientPersonFormFragment extends Fragment implements View.OnClickListener {

    private ClientViewModel clientViewModel;


    private EditText firstNameView;
    private EditText lastNameView;
    private EditText emailView;
    private TextView errorView;
    private FloatingActionButton fabValid;

    private Gender gender; // DATA to set from ClientFormFragment
    private String firstName;
    private String lastName;
    private String email;

    private final String FIRST_NAME_SAVE = "FIRST_NAME";
    private final String LAST_NAME_SAVE = "LAST_NAME";
    private final String EMAIL_SAVE = "EMAIL_SAVE";

    public void refreshScreen() {
        this.firstNameView.setText(this.firstName);
        this.lastNameView.setText(this.lastName);
        this.emailView.setText(this.email);
    }


    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.clientViewModel = ViewModelProviders.of(this).get(ClientViewModel.class);

        View root = inflater.inflate(R.layout.fragment_client_person_form, container, false);

        this.firstNameView = root.findViewById(R.id.client_first_name);
        this.lastNameView = root.findViewById(R.id.client_last_name);
        this.emailView = root.findViewById(R.id.email);
        this.errorView = root.findViewById(R.id.error);
        this.fabValid = root.findViewById(R.id.fab_valid);

        this.fabValid.setOnClickListener(this);


        this.clientViewModel.getClient().observe(this, new Observer<Client>() {
            @Override
            public void onChanged(Client client) {
                if(client != null) {
                    if(client.getClient() != null) {
                        if(client.getClient() instanceof Person) {
                            firstName = ((Person) client.getClient()).getFirstName();
                            lastName = ((Person) client.getClient()).getLastName();
                            email = ((Person) client.getClient()).getEmail().getAddress();
                        }
                    }
                }
            }
        });


        if(savedInstanceState != null) {
            this.firstName = savedInstanceState.getString(FIRST_NAME_SAVE);
            this.lastName = savedInstanceState.getString(LAST_NAME_SAVE);
            this.email = savedInstanceState.getString(EMAIL_SAVE);
            this.refreshScreen();
        }

        return root;

    }

    @Override
    public void onResume() {
        super.onResume();
        this.firstNameView.setText(this.firstName);
        this.lastNameView.setText(this.lastName);
        this.emailView.setText(this.email);

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) { // Doesn't Work
        super.onSaveInstanceState(outState);
        outState.putString(this.FIRST_NAME_SAVE, this.firstName);
        outState.putString(this.LAST_NAME_SAVE, this.lastName);
        outState.putString(this.EMAIL_SAVE, this.email);
    }

    @Override
    public void onClick(View v) {
        Client client = null;

        if(TextUtils.isEmpty(this.firstNameView.getText())) {
            this.errorView.setText(getContext().getString(R.string.error_first_name));
        } else if(TextUtils.isEmpty(this.lastNameView.getText())) {
            this.errorView.setText(getContext().getString(R.string.error_last_name));
        } else if(TextUtils.isEmpty(this.emailView.getText())) {
            this.errorView.setText(getContext().getString(R.string.error_email));
        } else {
            // Clear the error message
            this.errorView.setText("");

            // instanciate person as client
            Person person = new Person(this.gender, this.firstNameView.getText().toString(), this.lastNameView.getText().toString());
            // set Email
            try {
                InternetAddress email = new InternetAddress(emailView.getText().toString());
                person.setEmail(email);

                // set person as Client
                client = new Client<Person>();
                client.setClient(person);

                // Valid Client ***************************************
                this.clientViewModel.validClient(client);

                // Show Success ****************************************
                SuccessFragment successFragment = new SuccessFragment();
                successFragment.setSuccessType(SuccessType.CLIENT);
                getParentFragment().getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frame_of_client_fragment, successFragment)
                        .commit(); // VERY OK without add to back stack > returns to cart

            } catch (AddressException e) {
                errorView.setText(getContext().getString(R.string.error_email_invalid));
            }
        }

    }
}
