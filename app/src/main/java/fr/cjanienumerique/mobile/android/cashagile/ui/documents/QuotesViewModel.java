package fr.cjanienumerique.mobile.android.cashagile.ui.documents;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.io.IOException;
import java.util.List;

import javax.mail.internet.AddressException;

import fr.cjanienumerique.mobile.android.cashagile.entities.Quote;
import fr.cjanienumerique.mobile.android.cashagile.managers.FileStorageManagerSession;
import fr.cjanienumerique.mobile.android.cashagile.repositories.QuoteRepository;

public class QuotesViewModel extends AndroidViewModel {

    private QuoteRepository quoteRepository;

    public QuotesViewModel(@NonNull Application application) {
        super(application);
        this.quoteRepository = QuoteRepository.getInstance();
    }

    public MutableLiveData<List<Quote>> getQuotes() throws IOException, AddressException {
        return this.quoteRepository.getQuotes(FileStorageManagerSession.getInstance(this.getApplication()));
    }
}
