package fr.cjanienumerique.mobile.android.cashagile.managers;


import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class FileWriterManager {



    private static FileWriterManager INSTANCE;

    private FileWriterManager() {

    }

    public static FileWriterManager getInstance() {
        if(FileWriterManager.INSTANCE == null) {
            FileWriterManager.INSTANCE = new FileWriterManager();
        }
        return FileWriterManager.INSTANCE;
    }


    public void write(String string, FileStorageManager fileStorageManager) throws IOException {
        FileWriter writer = null;
        try {
            writer = new FileWriter(fileStorageManager.getTargetFile());
            writer.append(string);
            writer.append('\n');
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            if(writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    throw e;
                }
            }
        }
    }

    public void write(List<String> strings, FileStorageManager fileStorageManager) throws IOException {
        FileWriter writer = null;
        try {
            writer = new FileWriter(fileStorageManager.getTargetFile());
            for(String string: strings) {
                writer.append(string);
                writer.append('\n');
            }
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if(writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
        }
    }

}
