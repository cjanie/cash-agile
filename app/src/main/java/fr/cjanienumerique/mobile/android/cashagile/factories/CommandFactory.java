package fr.cjanienumerique.mobile.android.cashagile.factories;

import fr.cjanienumerique.mobile.android.cashagile.entities.Command;
import fr.cjanienumerique.mobile.android.cashagile.entities.Item;
import fr.cjanienumerique.mobile.android.cashagile.exceptions.EmptyParameterException;
import fr.cjanienumerique.mobile.android.cashagile.exceptions.InvalidPriceException;
import fr.cjanienumerique.mobile.android.cashagile.exceptions.InvalidQuantityException;

public class CommandFactory {

    private static CommandFactory INSTANCE;

    private CommandFactory() {}

    public static CommandFactory getInstance() {
        if(CommandFactory.INSTANCE == null) {
            CommandFactory.INSTANCE = new CommandFactory();
        }
        return CommandFactory.INSTANCE;
    }

    public Command create(String quantityFromInput, String itemNameFromInput, String unityPriceFromInput) throws InvalidQuantityException, InvalidPriceException, EmptyParameterException {
        Command command = null;
        Integer quantity = null;
        String itemName = null;
        Float unityPrice = null;

        // check if quantity from input is valid
        try {
            quantity = Integer.parseInt(quantityFromInput);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            throw new InvalidQuantityException();
        }

        // check if unity price from input is valid
        try {
            unityPrice = Float.parseFloat(unityPriceFromInput);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            throw new InvalidPriceException();
        }

        itemName = itemNameFromInput.trim();
        if(itemName.length() > 0) {
            itemName = itemNameFromInput;
        } else {
            itemName = null;
            throw new EmptyParameterException();
        }

        // create command if valid
        if(quantity != null && itemName != null && unityPrice != null) {
            command = new Command(quantity, new Item(itemNameFromInput, unityPrice));
        }

        return command;
    }
}
