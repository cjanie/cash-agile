package fr.cjanienumerique.mobile.android.cashagile.ui.client;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.IOException;

import javax.mail.internet.AddressException;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.entities.Client;
import fr.cjanienumerique.mobile.android.cashagile.entities.Edition;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.ClientInterface;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.Editable;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.SuccessMessageBuilder;
import fr.cjanienumerique.mobile.android.cashagile.services.EditionService;

public class ClientViewModel extends AndroidViewModel implements SuccessMessageBuilder {

    private EditionService editionService;

    public ClientViewModel(@NonNull Application application) {
        super(application);
        this.editionService = EditionService.getInstance();
    }

    public void validClient(Client client) {
        this.editionService.setClient(client);
    }

    public MutableLiveData<Client> getClient() {
        MutableLiveData<Client> client = new MutableLiveData<Client>();
        Edition edition = this.editionService.getEdition().getValue();
        if(edition != null) {
            if(edition.getClient() != null) {
                client.setValue(edition.getClient());
            }
        }
        return client;
    }

    @Override
    public String buildSuccessMessage(Editable editable) {
        StringBuilder stringBuilder = new StringBuilder(this.getApplication().getApplicationContext()
                .getString(R.string.client) + ": ");
        stringBuilder.append((((ClientInterface) ((Client) editable).getClient()).formatClientToString()));
        return stringBuilder.toString();
    }

}