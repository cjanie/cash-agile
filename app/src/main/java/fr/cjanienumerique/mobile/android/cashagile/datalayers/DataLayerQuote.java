package fr.cjanienumerique.mobile.android.cashagile.datalayers;




import androidx.lifecycle.MutableLiveData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;

import fr.cjanienumerique.mobile.android.cashagile.entities.Quote;
import fr.cjanienumerique.mobile.android.cashagile.entities.Session;
import fr.cjanienumerique.mobile.android.cashagile.managers.FileStorageManagerSession;
import fr.cjanienumerique.mobile.android.cashagile.repositories.SessionRepository;

public class DataLayerQuote {

    private SessionRepository sessionRepository;

    private static DataLayerQuote INSTANCE;

    private DataLayerQuote() {
        this.sessionRepository = SessionRepository.getInstance();
    }

    public static DataLayerQuote getInstance() {
        if(DataLayerQuote.INSTANCE == null) {
            DataLayerQuote.INSTANCE = new DataLayerQuote();
        }
        return DataLayerQuote.INSTANCE;
    }

    public void saveQuote(Quote quote, FileStorageManagerSession fileStorageManager) throws IOException, AddressException {
        Session session = this.sessionRepository.getSession(fileStorageManager).getValue();
        if(session != null) {
            if(session.getQuotes() != null) {
                List<Quote> quotes = session.getQuotes();
                if(quotes != null) {
                    quote.setId(quotes.size() + 1);
                    quotes.add(quote);

                } else {
                    quotes = new ArrayList<>();
                    quote.setId(quotes.size() + 1);
                    quotes.add(quote);
                }
                session.setQuotes(quotes);
                this.sessionRepository.saveSession(session, fileStorageManager);
            }
        }

    }

    public MutableLiveData<List<Quote>> getQuotes(FileStorageManagerSession fileStorageManagerSession) throws IOException, AddressException {
        MutableLiveData<List<Quote>> quotes = new MutableLiveData<>();
        Session session = this.sessionRepository.getSession(fileStorageManagerSession).getValue();
        if(session != null) {
            quotes.setValue(session.getQuotes());
        }
        return quotes;
    }

    public MutableLiveData<Quote> getLastQuote(FileStorageManagerSession fileStorageManagerSession) throws IOException, AddressException {
        MutableLiveData<Quote> quote = new MutableLiveData<>();
        Session session = this.sessionRepository.getSession(fileStorageManagerSession).getValue();
        if(session != null) {
            if(session.getQuotes() != null) {
                if(!session.getQuotes().isEmpty()) {
                    Quote quoteObject = session.getQuotes().get(session.getQuotes().size() - 1);
                    quote.setValue(quoteObject);
                }
            }

        }
        return quote;
    }


}
