package fr.cjanienumerique.mobile.android.cashagile.ui.edition;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.IOException;

import javax.mail.internet.AddressException;

import fr.cjanienumerique.mobile.android.cashagile.entities.Edition;
import fr.cjanienumerique.mobile.android.cashagile.services.EditionService;

public class EditionViewModel extends ViewModel {

    private EditionService editionService;

    public EditionViewModel(@NonNull Application application) {
        super();
        this.editionService = EditionService.getInstance();
    }


    public MutableLiveData<Edition> getEdition() {
        return this.editionService.getEdition();
    }


}
