package fr.cjanienumerique.mobile.android.cashagile.managers;

import android.app.Application;

import java.io.File;

import fr.cjanienumerique.mobile.android.cashagile.enums.FileTarget;

public abstract class FileStorageManager {

    protected File parent;
    protected String parentPath;
    protected String childName;

    protected FileTarget fileTarget;


    protected FileStorageManager(Application application) {
        this.parent = application.getFilesDir();
        this.parentPath = this.parent.getPath();
        this.childName = "text";
    }



    public File getTargetFile() {
        File container = new File(this.parent, this.childName);
        if(!container.exists()) {
            container.mkdir();
        }
        File targetFile = new File(container, this.getTargetFileName());
        return targetFile;
    }

    public String getTargetFilePath() {
        return this.parentPath + "/" + this.childName + "/" + this.getTargetFileName();
    }

    public abstract String getTargetFileName();


}
