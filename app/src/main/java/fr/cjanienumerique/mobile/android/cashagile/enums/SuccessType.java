package fr.cjanienumerique.mobile.android.cashagile.enums;

public enum SuccessType {
    CART, CLIENT, COMPANY
}
