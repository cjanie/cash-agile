package fr.cjanienumerique.mobile.android.cashagile.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.adapters.CommandAdapter;
import fr.cjanienumerique.mobile.android.cashagile.entities.Cart;
import fr.cjanienumerique.mobile.android.cashagile.ui.cart.CartViewModel;

public class CashDeskBoardFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    private CartViewModel cartViewModel;

    private Integer selectedCommandIndex;

    private TextView successCart;
    private GridView commandItems;
    private GridLayout cartGridLayout;
    private TextView cartTotal;
    private TextView errorNoSelection;

    private FloatingActionButton fabAddCommand;
    private FloatingActionButton fabModifyCommand;
    private FloatingActionButton fabDeleteCommand;

    private void loadData() {
        this.cartViewModel.getCart().observe(this, new Observer<Cart>() {
            @Override
            public void onChanged(Cart cart) {

                if(cart != null) {
                    if(!cart.getCommands().isEmpty()) {
                        successCart.setText(cartViewModel.buildSuccessMessage(cart));
                        CommandAdapter commandAdapter = new CommandAdapter(getContext(), 0, cart.getCommands());
                        commandItems.setAdapter(commandAdapter);
                        cartTotal.setText(getContext().getString(R.string.total) + ": " + cart.getTotal().toString());
                        cartGridLayout.setRowCount(cart.getCommands().size() + 1); // + 1 for label
                    }
                }
            }
        });
    }

    private void refreshScreen() {
        this.loadData();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.cartViewModel = ViewModelProviders.of(this).get(CartViewModel.class);
        View root = inflater.inflate(R.layout.fragment_cash_desk_board, container, false);
        this.successCart = root.findViewById(R.id.success_cart);
        this.commandItems = root.findViewById(R.id.command_items);
        this.cartGridLayout = root.findViewById(R.id.cart_grid);
        this.cartTotal = root.findViewById(R.id.cart_total);
        this.errorNoSelection = root.findViewById(R.id.error_no_selection);
        this.fabAddCommand = root.findViewById(R.id.fab_add);
        this.fabModifyCommand = root.findViewById(R.id.fab_modify);
        this.fabDeleteCommand = root.findViewById(R.id.fab_delete);

        this.loadData();

        this.commandItems.setOnItemClickListener(this);

        this.fabAddCommand.setOnClickListener(this);
        this.fabModifyCommand.setOnClickListener(this);
        this.fabDeleteCommand.setOnClickListener(this);



        return root;
    }

    @Override
    public void onClick(View v) {
        if(v == this.fabAddCommand) {
            this.selectedCommandIndex = null;
            CommandFormFragment commandFormFragment = new CommandFormFragment();
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_of_cart_fragment, commandFormFragment)
                    .commit();

        } else if(v == this.fabModifyCommand) {
            if(this.selectedCommandIndex == null) {
                this.errorNoSelection.setText(getContext().getString(R.string.error_selection_item));
            } else {
                CommandFormFragment commandFormFragment = new CommandFormFragment();
                commandFormFragment.setSelectedCommandIndex(this.selectedCommandIndex);
                this.selectedCommandIndex = null;
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frame_of_cart_fragment, commandFormFragment)
                        .commit();

            }

        } else if(v == this.fabDeleteCommand) {
            if(this.selectedCommandIndex == null) {
                this.errorNoSelection.setText(getContext().getString(R.string.error_selection_item));
            } else {
                    ConfirmFragment confirmFragment = new ConfirmFragment();
                    confirmFragment.setConfirmMessage("Voulez-vous supprimer l'item?");
                    confirmFragment.setSelectedCommandIndex(this.selectedCommandIndex);
                    this.getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.frame_of_cart_fragment, confirmFragment)
                            .commit();
            }
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        view.setBackgroundColor(getResources().getColor(R.color.colorListSelection));
        this.errorNoSelection.setText("");
        this.selectedCommandIndex = position;
    }
}
