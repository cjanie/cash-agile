package fr.cjanienumerique.mobile.android.cashagile.managers;

import android.graphics.pdf.PdfDocument;
import android.os.Build;
import android.view.View;

import androidx.annotation.RequiresApi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

// https://developer.android.com/reference/android/graphics/pdf/PdfDocument
// https://programmerworld.co/android/how-to-create-pdf-file-in-your-android-app-complete-source-code-using-android-studio/

public class PdfMaker {

    protected File parent;
    protected String parentPath;
    protected String childName;



    private static PdfMaker INSTANCE;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private PdfMaker() {
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static PdfMaker getInstance() {
        if(PdfMaker.INSTANCE == null) {
            PdfMaker.INSTANCE = new PdfMaker();
        }
        return PdfMaker.INSTANCE;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void makePdf(View view, PdfStorage pdfStorage) throws IOException {

        PdfDocument pdfDocument = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(900, 900, 1).create();
        PdfDocument.Page page = pdfDocument.startPage(pageInfo);
        view.draw(page.getCanvas());
        pdfDocument.finishPage(page);
        File file = pdfStorage.getTargetFile();
        pdfDocument.writeTo(new FileOutputStream(file));
        pdfDocument.close();
    }
}
