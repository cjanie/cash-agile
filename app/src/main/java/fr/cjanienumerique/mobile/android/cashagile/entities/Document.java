package fr.cjanienumerique.mobile.android.cashagile.entities;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.time.LocalDate;

import fr.cjanienumerique.mobile.android.cashagile.converters.EditionConverter;
import fr.cjanienumerique.mobile.android.cashagile.converters.LocalDateConverter;
import fr.cjanienumerique.mobile.android.cashagile.enums.DocumentType;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.Convertible;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.JsonInterface;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.Loadable;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.PdfInterface;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.Printable;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.Saveable;

public abstract class Document implements Convertible, Printable {


    protected Integer id;

    protected String title;

    protected LocalDate dateOfEdition;

    protected Edition edition;

    protected Boolean sent;

    // Constructor

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Document(Edition edition) {
        this.title = "";
        this.dateOfEdition = LocalDate.now();
        this.edition = edition;
        this.sent = false;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getDateOfEdition() {
        return dateOfEdition;
    }

    public void setDateOfEdition(LocalDate dateOfEdition) {
        this.dateOfEdition = dateOfEdition;
    }

    public Edition getEdition() {
        return edition;
    }

    public void setEdition(Edition edition) {
        this.edition = edition;
    }

    public Boolean getSent() {
        return sent;
    }

    public void setSent(Boolean sent) {
        this.sent = sent;
    }

}
