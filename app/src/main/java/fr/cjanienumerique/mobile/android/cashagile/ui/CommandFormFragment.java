package fr.cjanienumerique.mobile.android.cashagile.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.entities.Cart;
import fr.cjanienumerique.mobile.android.cashagile.entities.Command;
import fr.cjanienumerique.mobile.android.cashagile.exceptions.EmptyParameterException;
import fr.cjanienumerique.mobile.android.cashagile.exceptions.InvalidPriceException;
import fr.cjanienumerique.mobile.android.cashagile.exceptions.InvalidQuantityException;
import fr.cjanienumerique.mobile.android.cashagile.factories.CommandFactory;
import fr.cjanienumerique.mobile.android.cashagile.ui.cart.CartFragment;
import fr.cjanienumerique.mobile.android.cashagile.ui.cart.CartViewModel;

public class CommandFormFragment extends Fragment implements View.OnClickListener {

    private CartViewModel cartViewModel;


    // form views to clear
    private EditText quantityView;
    private EditText itemNameView;
    private EditText unityPriceView;
    private TextView totalView;
    private TextView errorView;
    private FloatingActionButton fabValidCommand;

    // data
    private Integer quantity;
    private String itemName;
    private Float unityPrice;
    private Float total;

    private Integer selectedCommandIndex; // TODO


    // Setters

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public void setUnityPrice(Float unityPrice) {
        this.unityPrice = unityPrice;
    }



    public void setSelectedCommandIndex(Integer selectedCommandIndex) {
        this.selectedCommandIndex = selectedCommandIndex;
    }

    private void validCommand() throws Exception {
        if(TextUtils.isEmpty(this.quantityView.getText())) {
            this.errorView.setText(getContext().getString(R.string.error_quantity_empty));
        } else if(TextUtils.isEmpty(this.itemNameView.getText())) {
            this.errorView.setText(getContext().getString(R.string.error_item_name_empty));
        } else if(TextUtils.isEmpty(this.unityPriceView.getText())) {
            this.errorView.setText(getContext().getString(R.string.error_unity_price_empty));
        } else {

            // clear the error message
            this.errorView.setText("");

            // instanciate Command from inputs using factory

            try {
                // make Command using factory
                Command command = CommandFactory.getInstance().create(
                        this.quantityView.getText().toString(),
                        this.itemNameView.getText().toString(),
                        this.unityPriceView.getText().toString()
                );

                // add command to Cart or update Cart
                if(this.selectedCommandIndex == null) {
                    this.cartViewModel.addToCart(command);
                } else {
                    this.cartViewModel.updateCart(this.selectedCommandIndex, command); // TODO
                    this.selectedCommandIndex = null;
                }

            } catch (InvalidQuantityException e) {
                e.printStackTrace();
                errorView.setText(getContext().getString(R.string.error_quantity_invalid));
            } catch (InvalidPriceException e) {
                e.printStackTrace();
                errorView.setText(getContext().getString(R.string.error_unity_price_invalid));
            } catch (EmptyParameterException e) {
                e.printStackTrace();
                errorView.setText(getContext().getString(R.string.error_item_name_empty));
            }

            // observe cart to set the cash desk grid
            cartViewModel.getCart().observe(this, new Observer<Cart>() {
                @Override
                public void onChanged(Cart cart) {

                    if(cart != null) {
                        getFragmentManager()
                                .beginTransaction()
                                .replace(R.id.frame_of_cart_fragment, new CashDeskBoardFragment())
                                .commit();
                    }

                }
            });
        }
    }

    private void clearForm() {
        this.quantityView.setText("");
        this.itemNameView.setText("");
        this.unityPriceView.setText("");
        this.totalView.setText("");
    }

    private void observeCartToGetTheCommandToUpdate() {
        this.cartViewModel.getCart().observe(this, new Observer<Cart>() {
            @Override
            public void onChanged(Cart cart) {
                if(cart != null) {
                    if(selectedCommandIndex != null) {
                        errorView.setText("");
                        quantityView.setText(cart.getCommands().get(selectedCommandIndex).getQuantity().toString());
                        itemNameView.setText(cart.getCommands().get(selectedCommandIndex).getItem().getName());
                        unityPriceView.setText(cart.getCommands().get(selectedCommandIndex).getItem().getPrice().toString());
                        totalView.setText(cart.getCommands().get(selectedCommandIndex).getTotalCommand().toString());
                    }
                }



            }
        });
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.cartViewModel = ViewModelProviders.of(this).get(CartViewModel.class);

        View root = inflater.inflate(R.layout.fragment_command_form, container, false);

        this.quantityView = root.findViewById(R.id.command_quantity);
        this.itemNameView = root.findViewById(R.id.command_item_name);
        this.unityPriceView = root.findViewById(R.id.command_unity_price);
        this.totalView = root.findViewById(R.id.total_command);

        this.errorView = root.findViewById(R.id.error);
        this.fabValidCommand = root.findViewById(R.id.fab_valid);
        this.fabValidCommand.setOnClickListener(this);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(this.selectedCommandIndex != null) {
            this.observeCartToGetTheCommandToUpdate();
        }

    }

    @Override
    public void onClick(View v) {
        try {
            this.validCommand();
        } catch (Exception e) {
            e.printStackTrace();
            this.errorView.setText(e.getMessage());
        }
    }
}
