package fr.cjanienumerique.mobile.android.cashagile.deserializers;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.time.LocalDate;

import fr.cjanienumerique.mobile.android.cashagile.converters.EditionConverter;
import fr.cjanienumerique.mobile.android.cashagile.converters.LocalDateConverter;
import fr.cjanienumerique.mobile.android.cashagile.entities.Edition;
import fr.cjanienumerique.mobile.android.cashagile.entities.Quote;

public class QuoteDeserializer implements JsonDeserializer<Quote> {

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public Quote deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if(json == null) {
            return null;
        } else {
            Integer id = json.getAsJsonObject().get("id").getAsInt();
            LocalDate dateOfEdition = LocalDateConverter.fromLong(json.getAsJsonObject().get("date_of_edition").getAsLong());
            Edition edition = EditionConverter.jsonToEdition(json.getAsJsonObject().get("edition").getAsString());
            Boolean sent = json.getAsJsonObject().get("sent").getAsBoolean();
            Boolean purchaseOrder = json.getAsJsonObject().get("purchase_order").getAsBoolean();

            Quote quote = new Quote(edition);
            quote.setId(id);
            quote.setDateOfEdition(dateOfEdition);
            quote.setSent(sent);
            quote.setPurchaseOrder(purchaseOrder);
            return quote;
        }

    }
}
