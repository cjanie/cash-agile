package fr.cjanienumerique.mobile.android.cashagile.interfaces;

import android.graphics.pdf.PdfDocument;

public interface PdfInterface {

    PdfDocument writePdf();

    void openPdf();
}
