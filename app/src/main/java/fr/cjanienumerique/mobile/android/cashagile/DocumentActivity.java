package fr.cjanienumerique.mobile.android.cashagile;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.pdf.PdfDocument;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Environment;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import fr.cjanienumerique.mobile.android.cashagile.adapters.CommandAdapter;
import fr.cjanienumerique.mobile.android.cashagile.entities.Document;
import fr.cjanienumerique.mobile.android.cashagile.entities.Quote;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.ClientInterface;
import fr.cjanienumerique.mobile.android.cashagile.ui.DocumentViewModel;

public class DocumentActivity extends AppCompatActivity {

    private DocumentViewModel documentViewModel;





    private View quoteFragment;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.documentViewModel = ViewModelProviders.of(this).get(DocumentViewModel.class);

        ActivityCompat.requestPermissions(DocumentActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);

        final TextView companyName = this.findViewById(R.id.document_company_name);
        final TextView companyReference = this.findViewById(R.id.document_company_reference);
        final TextView clientName = this.findViewById(R.id.document_client_name);
        final TextView title = this.findViewById(R.id.document_title);
        final TextView date = this.findViewById(R.id.document_date);
        final GridView commandItems = this.findViewById(R.id.command_items);
        final TextView total = this.findViewById(R.id.cart_total);
        final FloatingActionButton fabEmail = findViewById(R.id.fab_email);
        final FloatingActionButton fabPdf = findViewById(R.id.fab_pdf);
        final ProgressBar progressBar = findViewById(R.id.indeterminateBar);
        final FloatingActionButton fabClose = findViewById(R.id.fab_close_document);
        this.quoteFragment = this.findViewById(R.id.quote);


        try {
            this.getLastQuote().observe(this, new Observer<Quote>() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onChanged(Quote quote) {
                    if(quote != null) {
                        companyName.setText(getApplicationContext().getString(R.string.from) + " " + quote.getEdition().getCompany().formatCompanyToString());
                        companyReference.setText(getApplicationContext().getString(R.string.company_reference) + ": " + quote.getEdition().getCompany().getReference());
                        // java.lang.ClassCastException: com.google.gson.internal.LinkedTreeMap cannot be cast to fr.cjanienumerique.mobile.android.cashagile.interfaces.ClientInterface
                        clientName.setText(getApplicationContext().getString(R.string.to) + " " + ((ClientInterface) quote.getEdition().getClient().getClient()).formatClientToString());
                        title.setText(getApplicationContext().getString(R.string.quote) + " n° " + quote.getId());
                        date.setText(getApplicationContext().getString(R.string.of) + " " + quote.getDateOfEdition().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));
                        CommandAdapter commandAdapter = new CommandAdapter(getApplicationContext(), 0, quote.getEdition().getCart().getCommands());
                        commandItems.setAdapter(commandAdapter);
                        total.setText(getApplicationContext().getString(R.string.total) + ": " + quote.getEdition().getCart().getTotal().toString());

                        System.out.println("quote printAsString: " + quote.printAsString());



                        fabEmail.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Snackbar.make(v, "Send Email", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();

                                try {
                                    sendEmail(quote);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (MessagingException e) {
                                    e.printStackTrace();
                                }

                            }
                        });

                        fabPdf.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                //createPdf(quote);
                                try {
                                    makePdf();
                                    Snackbar.make(v, "Pdf in phone storage", Snackbar.LENGTH_LONG)
                                            .setAction("Action", null).show();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    Snackbar.make(v, e.getMessage(), Snackbar.LENGTH_LONG)
                                            .setAction("Action", null).show();
                                }
                            }
                        });
                    }

                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (AddressException e) {
            e.printStackTrace();
        }

        fabClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }




    private MutableLiveData<Quote> getLastQuote() throws IOException, AddressException {
        return this.documentViewModel.getLastQuote();
    }

    // email function https://www.tutorialspoint.com/how-to-send-email-on-android-using-javamail-api

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void sendEmail(Document document) throws IOException, MessagingException {
        this.documentViewModel.sendEmail(document);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void createPdf(Document document) {

        // https://programmerworld.co/android/how-to-create-pdf-file-in-your-android-app-complete-source-code-using-android-studio/
        PdfDocument pdfDocument = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(900, 900, 1).create();
        PdfDocument.Page page = pdfDocument.startPage(pageInfo);
        View view = this.quoteFragment;
        view.draw(page.getCanvas());
        pdfDocument.finishPage(page);

        //https://developer.android.com/reference/android/content/Context#getExternalFilesDir(java.lang.String)
        String filePath = this.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).getPath() + "/document.pdf";
        File file = new File(filePath);
        try {
            pdfDocument.writeTo(new FileOutputStream(file));
            System.out.println(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        pdfDocument.close();


    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void makePdf() throws IOException {
        this.documentViewModel.makePdf(this.quoteFragment);
    }



    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }


}
