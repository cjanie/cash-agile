package fr.cjanienumerique.mobile.android.cashagile;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.io.IOException;

import javax.mail.internet.AddressException;


import fr.cjanienumerique.mobile.android.cashagile.entities.Company;
import fr.cjanienumerique.mobile.android.cashagile.entities.Edition;

import fr.cjanienumerique.mobile.android.cashagile.entities.Quote;

import fr.cjanienumerique.mobile.android.cashagile.entities.Session;
import fr.cjanienumerique.mobile.android.cashagile.ui.MainViewModel;
import fr.cjanienumerique.mobile.android.cashagile.ui.company.CompanyViewModel;

public class MainActivity extends FragmentActivity implements NavController.OnDestinationChangedListener, View.OnClickListener {

    private MainViewModel mainViewModel;

    private NavController navController;

    private View navCart;
    private View navClient;
    private View navCompany;

    private FloatingActionButton fabPlay;


    // functions related to EditionService using MainViewModel

    private void resetEditionService() {
        this.mainViewModel.resetEditionService();
    }

    private MutableLiveData<Edition> getEdition() {
        return this.mainViewModel.getEdition();
    }

    private void saveQuote(Quote quote) throws IOException, AddressException {
        this.mainViewModel.saveQuote(quote);
    }


    // functions to set colors on navbar

    private void setNavViewColorUndone(View navView) {
        navView.setBackgroundColor(this.getResources().getColor(R.color.yellow));
    }

    private void setNavViewColorSuccess(View navView) {
        navView.setBackgroundColor(this.getResources().getColor(R.color.colorSuccess));
    }

    private void setCartColor(Edition edition) {
        if(edition != null) {
            if(edition.getCart() != null) {
                if(!edition.getCart().getCommands().isEmpty()) {
                    setNavViewColorSuccess(this.navCart);
                } else {setNavViewColorUndone(this.navCart);}
            } else {
                setNavViewColorUndone(this.navCart);
            }
        }
    }

    private void setClientColor(Edition edition) {
        if(edition != null) {
            if(edition.getClient() != null) {
                setNavViewColorSuccess(this.navClient);
            } else {
                setNavViewColorUndone(this.navClient);
            }
        }
    }

    private void setCompanyColor(Edition edition) {
        if(edition != null) {
            if(edition.getCompany() != null) {
                setNavViewColorSuccess(this.navCompany);
            } else {
                setNavViewColorUndone(this.navCompany);
            }
        }
    }

    private void setNavColors() {

        this.getEdition().observe(this, new Observer<Edition>() {
            @Override
            public void onChanged(Edition edition) {
                setCartColor(edition);
                setClientColor(edition);
                setCompanyColor(edition);
            }
        });

    }

    private void observeEditionToNavigate() {
        this.getEdition().observe(MainActivity.this, new Observer<Edition>() {
            @Override
            public void onChanged(Edition edition) {
                if(edition.getCart() == null) {
                    navController.navigate(R.id.navigation_cart);

                } else if(edition.getCart().getCommands().isEmpty()) {
                    navController.navigate(R.id.navigation_cart);

                } else if(edition.getClient() == null) {
                    navController.navigate(R.id.navigation_client);


                } else if(edition.getCompany() == null) {
                    CompanyViewModel companyViewModel = ViewModelProviders.of(MainActivity.this).get(CompanyViewModel.class);
                    try {
                        companyViewModel.getSession().observe(MainActivity.this, new Observer<Session>() {
                            @Override
                            public void onChanged(Session session) {
                                Company company = session.getSession();
                                if(company == null) {
                                    navController.navigate(R.id.navigation_company);

                                } else {
                                    companyViewModel.setCompanyToEdition(company);
                                    navController.navigate(R.id.navigation_company);
                                    setNavColors();
                                }
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (AddressException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void observeEditionToEdit() {
        this.getEdition().observe(this, new Observer<Edition>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onChanged(Edition edition) {
                if(edition != null) {
                    if(edition.getCart() != null && edition.getClient() != null && edition.getCompany() != null) {
                        if(edition.getCart().getCommands() != null) {
                            if(!edition.getCart().getCommands().isEmpty()) {
                                Quote quote = new Quote(edition);
                                try {
                                    saveQuote(quote);
                                    Intent intent = new Intent(getApplicationContext(), DocumentActivity.class);
                                    startActivity(intent);
                                    finish();

                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (AddressException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        });
    }


    // Override Activity


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        this.mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);


        // View *************

        this.setContentView(R.layout.activity_main);

        BottomNavigationView navView = findViewById(R.id.nav_view);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_cart, R.id.navigation_client, R.id.navigation_company)
                .build();
        this.navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navView, navController);

        this.navCart = findViewById(R.id.navigation_cart);
        this.navClient = findViewById(R.id.navigation_client);
        this.navCompany = findViewById(R.id.navigation_company);

        this.fabPlay = this.findViewById(R.id.fab_play);

        // addListeners
        // to set Color Accent on the destination
        this.navController.addOnDestinationChangedListener(this);

        // to set Color Success or Undone in navbar with observer
        this.navCart.setOnClickListener(this);
        this.navClient.setOnClickListener(this);
        this.navCompany.setOnClickListener(this);

        // to make quote if ready (observer)
        fabPlay.setOnClickListener(this);

        if(savedInstanceState == null) {
            this.resetEditionService();
        } else {
            this.setNavColors();
        }
    }

    @Override
    public void onBackPressed() {
        this.getSupportFragmentManager().popBackStack();
    }


    // Override OnDestinationChanged

    @Override
    public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
        if(destination.getId() == R.id.navigation_cart) {
            this.navCart.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        } else if(destination.getId() == R.id.navigation_client) {
            this.navClient.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        } else if(destination.getId() == R.id.navigation_company) {
            this.navCompany.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        }
    }

    // Override OnClick

    @Override
    public void onClick(View v) {
        if(v == this.navCart) {
            this.setNavColors();
            this.navController.navigate(R.id.navigation_cart);

        } else if(v == this.navClient) {
            this.setNavColors();
            this.navController.navigate(R.id.navigation_client);

        } else if(v == this.navCompany) {
            this.setNavColors();
            this.navController.navigate(R.id.navigation_company);

        } else if(v == this.fabPlay) {
            this.setNavColors();
            this.observeEditionToNavigate();
            this.observeEditionToEdit();
        }
    }


}
