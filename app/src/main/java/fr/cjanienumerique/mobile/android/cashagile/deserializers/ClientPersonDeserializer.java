package fr.cjanienumerique.mobile.android.cashagile.deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import fr.cjanienumerique.mobile.android.cashagile.converters.PersonConverter;
import fr.cjanienumerique.mobile.android.cashagile.entities.Person;
import fr.cjanienumerique.mobile.android.cashagile.enums.Gender;

public class ClientPersonDeserializer implements JsonDeserializer<Person> {
    @Override
    public Person deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if(json == null) {
            return null;
        } else {
            String jsonClientPerson = json.getAsJsonObject().get("client_person").getAsString();
            Person person = PersonConverter.jsonToPerson(jsonClientPerson);
            return person;
        }
    }
}
