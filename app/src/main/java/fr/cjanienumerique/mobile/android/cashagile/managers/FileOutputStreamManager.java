package fr.cjanienumerique.mobile.android.cashagile.managers;

import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutputStreamManager {

    private static FileOutputStreamManager INSTANCE;

    private FileOutputStreamManager() {}

    public static FileOutputStreamManager getInstance() {
        if(FileOutputStreamManager.INSTANCE == null) {
            FileOutputStreamManager.INSTANCE = new FileOutputStreamManager();
        }
        return FileOutputStreamManager.INSTANCE;
    }

    public String write(FileOutputStream fileOutputStream, String string) throws IOException {
        try {
            fileOutputStream.write(string.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            if(fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    throw e;
                }
            }
        }
        System.out.println("FileOutputStreamManager write done");
        return string;
    }
}
