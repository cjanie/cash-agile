package fr.cjanienumerique.mobile.android.cashagile.converters;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;


import fr.cjanienumerique.mobile.android.cashagile.deserializers.EditionDeserializer;
import fr.cjanienumerique.mobile.android.cashagile.entities.Client;
import fr.cjanienumerique.mobile.android.cashagile.entities.Company;
import fr.cjanienumerique.mobile.android.cashagile.entities.Edition;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.ClientInterface;
import fr.cjanienumerique.mobile.android.cashagile.serializers.EditionSerializer;

public class EditionConverter {

    @TypeConverter
    public static Edition jsonToEdition(String json) {
        if(json == null) {
            return null;
        } else {
            Gson gson = new GsonBuilder().registerTypeAdapter(Edition.class, new EditionDeserializer()).create();
            Edition edition = gson.fromJson(json, new TypeToken<Edition>() {}.getType());
            //Edition edition = gson.fromJson(json, Edition.class);
            return edition;
        }

    }

    @TypeConverter
    public static String editionToJson(Edition edition) {
        if(edition == null) {
            return null;
        } else {
            Gson gson = new GsonBuilder().registerTypeAdapter(Edition.class, new EditionSerializer()).create();
            String json = gson.toJson(edition, new TypeToken<Edition>() {}.getType());
            return json;
        }
    }



}
