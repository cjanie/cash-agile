package fr.cjanienumerique.mobile.android.cashagile.converters;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import fr.cjanienumerique.mobile.android.cashagile.deserializers.QuoteDeserializer;
import fr.cjanienumerique.mobile.android.cashagile.entities.Quote;
import fr.cjanienumerique.mobile.android.cashagile.serializers.QuoteSerialiser;

public class QuoteConverter {

    public static String quoteToJson(Quote quote) {
        if(quote == null) {
            return null;
        } else {
            Gson gson = new GsonBuilder().registerTypeAdapter(Quote.class, new QuoteSerialiser()).create();
            String json = gson.toJson(quote, new TypeToken<Quote>() {}.getType());
            return json;
        }
    }

    public static Quote jsonToQuote(String json) {
        if(json == null) {
            return null;
        } else {
            Gson gson = new GsonBuilder().registerTypeAdapter(Quote.class, new QuoteDeserializer()).create();
            Quote quote = gson.fromJson(json, new TypeToken<Quote>() {}.getType());
            return quote;
        }
    }
}
