package fr.cjanienumerique.mobile.android.cashagile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.entities.Client;
import fr.cjanienumerique.mobile.android.cashagile.entities.Quote;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.ClientInterface;

public class QuoteAdapter extends ArrayAdapter<Quote> {

    private List<Quote> quotes;

    public QuoteAdapter(@NonNull Context context, int resource, @NonNull List<Quote> quotes) {
        super(context, resource);
        this.quotes = quotes;
    }

    @Nullable
    @Override
    public Quote getItem(int position) {
        return this.quotes.get(position);
    }

    @Override
    public int getCount() {
        return this.quotes.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Quote quote = this.getItem(position);

        LayoutInflater inflater = LayoutInflater.from(getContext());

        View view = inflater.inflate(R.layout.model_document_item, null);
        ((TextView) view.findViewById(R.id.document_id)).setText(quote.getId().toString());
        Client<ClientInterface> client = quote.getEdition().getClient();
        ((TextView) view.findViewById(R.id.document_client)).setText(client.getClient().formatClientToString());
        ((TextView) view.findViewById(R.id.document_date_of_edition)).setText(quote.getDateOfEdition().toString());

        return view;
    }
}
