package fr.cjanienumerique.mobile.android.cashagile.managers;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BufferedReaderManager {

    private static BufferedReaderManager INSTANCE;

    private BufferedReaderManager() {

    }

    public static BufferedReaderManager getInstance() {
        if(BufferedReaderManager.INSTANCE == null) {
            BufferedReaderManager.INSTANCE = new BufferedReaderManager();
        }
        return BufferedReaderManager.INSTANCE;
    }

    public String read(FileStorageManager fileStorageManager) throws IOException{
        File fileEvents = new File(fileStorageManager.getTargetFilePath());
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileEvents));
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');

            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
        String result = text.toString();
        return result;
    }

    public List<String> readLines(FileStorageManager fileStorageManager) throws IOException {
        List<String> lines = new ArrayList<>();
        File fileEvents = new File(fileStorageManager.getTargetFilePath());
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader((fileEvents)));
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            if(br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    throw e;
                }
            }
        }
        return lines;
    }

}
