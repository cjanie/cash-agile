package fr.cjanienumerique.mobile.android.cashagile.serializers;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import fr.cjanienumerique.mobile.android.cashagile.converters.EditionConverter;
import fr.cjanienumerique.mobile.android.cashagile.converters.LocalDateConverter;
import fr.cjanienumerique.mobile.android.cashagile.entities.Quote;

public class QuoteSerialiser implements JsonSerializer<Quote> {


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public JsonElement serialize(Quote src, Type typeOfSrc, JsonSerializationContext context) {
        if(src == null) {
            return null;
        } else {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("id", src.getId());
            jsonObject.addProperty("date_of_edition", LocalDateConverter.fromLocalDate(src.getDateOfEdition()));
            jsonObject.addProperty("edition", EditionConverter.editionToJson(src.getEdition()));
            jsonObject.addProperty("sent", src.getSent());
            jsonObject.addProperty("purchase_order", src.getPurchaseOrder());
            return jsonObject;
        }

    }
}
