package fr.cjanienumerique.mobile.android.cashagile.ui.company;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.entities.Company;
import fr.cjanienumerique.mobile.android.cashagile.entities.Edition;
import fr.cjanienumerique.mobile.android.cashagile.entities.Quote;
import fr.cjanienumerique.mobile.android.cashagile.entities.Session;
import fr.cjanienumerique.mobile.android.cashagile.enums.FileTarget;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.Editable;
import fr.cjanienumerique.mobile.android.cashagile.interfaces.SuccessMessageBuilder;
import fr.cjanienumerique.mobile.android.cashagile.managers.FileStorageManager;
import fr.cjanienumerique.mobile.android.cashagile.managers.FileStorageManagerSession;
import fr.cjanienumerique.mobile.android.cashagile.repositories.SessionRepository;
import fr.cjanienumerique.mobile.android.cashagile.services.ConfigurationService;
import fr.cjanienumerique.mobile.android.cashagile.services.EditionService;

public class CompanyViewModel extends AndroidViewModel implements SuccessMessageBuilder {


    private SessionRepository sessionRepository;

    private FileStorageManagerSession fileStorageManager;

    private EditionService editionService;


    public CompanyViewModel(@NonNull Application application) {
        super(application);
        this.editionService = EditionService.getInstance();
        this.sessionRepository = SessionRepository.getInstance();
        this.fileStorageManager = FileStorageManagerSession.getInstance(application);

    }

    // To handle data using SessionRepository with FileStorageManager

    public void saveSession(Company company, String password) throws IOException, AddressException {
        Session existantSession = this.getSession().getValue();
        List<Quote> quotes = new ArrayList<>();
        if(existantSession != null) {
            quotes = existantSession.getQuotes();
        }
        Session session = new Session();
        session.setSession(company);
        session.setEmailPassword(password);
        session.setQuotes(quotes);
        this.sessionRepository.saveSession(session, this.fileStorageManager);
    }

    public MutableLiveData<Session> getSession() throws IOException, AddressException {
        return this.sessionRepository.getSession(this.fileStorageManager);
    }


    // Use EditionService which is the tool to edit a new document
    public void setCompanyToEdition(Company company) {
        this.editionService.setCompany(company);
    }



    // Use SuccessMessageBuilder to print the name of the validated company-session;

    @Override
    public String buildSuccessMessage(Editable editable) {
        return this.getApplication().getApplicationContext().getString(R.string.company) + ": " + ((Company) editable).formatCompanyToString();
    }
}