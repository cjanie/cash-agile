package fr.cjanienumerique.mobile.android.cashagile.ui.documents;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.io.IOException;
import java.util.List;

import javax.mail.internet.AddressException;

import fr.cjanienumerique.mobile.android.cashagile.R;
import fr.cjanienumerique.mobile.android.cashagile.adapters.QuoteAdapter;
import fr.cjanienumerique.mobile.android.cashagile.entities.Quote;

public class DocumentsFragment extends Fragment {

    private QuotesViewModel quotesViewModel;

    private GridView quoteItems;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.quotesViewModel = ViewModelProviders.of(this).get(QuotesViewModel.class);

        View root = inflater.inflate(R.layout.fragment_documents, container, false);

        this.quoteItems = root.findViewById(R.id.quote_items);

        try {
            this.getQuotes().observe(this, new Observer<List<Quote>>() {
                @Override
                public void onChanged(List<Quote> quotes) {
                    if(quotes != null) {
                        QuoteAdapter quoteAdapter = new QuoteAdapter(getContext(), 0, quotes);
                        quoteItems.setAdapter(quoteAdapter);
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (AddressException e) {
            e.printStackTrace();
        }


        return root;
    }

    private MutableLiveData<List<Quote>> getQuotes() throws IOException, AddressException {
        return this.quotesViewModel.getQuotes();
    }

    private void observeQuotesToFillGridView() {
        // TODO
    }
}
